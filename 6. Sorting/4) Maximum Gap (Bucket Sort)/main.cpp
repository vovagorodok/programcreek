#include <iostream>
#include <list>
#include <array>
#include <cmath>

struct Bucket
{
    int low;
    int high;
    bool initialized;
    Bucket() : initialized(false) {}
};

int maximumGap(const std::list<int>& list)
{
    if (list.size() < 2) return 0;
    
    int min = list.front();
    int max = list.front();
    for (int el : list)
    {
        min = std::min(min, el);
        max = std::max(max, el);
    }
    
    const double interval = static_cast<double>(list.size()) / (max - min);
    Bucket buckets[list.size() + 1]{};
    for (int el : list)
    {
        unsigned index = static_cast<unsigned>((el - min) * interval);
        if (not buckets[index].initialized)
        {
            buckets[index].low = el;
            buckets[index].high = el;
            buckets[index].initialized = true; 
        }
        else
        {
            buckets[index].low = std::min(buckets[index].low, el);
            buckets[index].high = std::max(buckets[index].high, el);
        }
    }
    
    int result = 0;
    int prev = buckets[0].high;
    for (const Bucket& bucket : buckets)
    {
        if (bucket.initialized)
        {
            result = std::max(result, bucket.low - prev);
            prev = bucket.high;
        }
    }
    
    return result;
}

int main()
{
    std::list<int> list = {3, 5, 5, 2, 9, 1, 66, 3, 2, 6, 8, 13, 5, 1};

    std::cout << "maximum gap: " << maximumGap(list) << std::endl;
    
    return 0;
}
