#include <iostream>
#include <vector>

void qicksort(std::vector<int>& vector, int first, int last)
{
    int i = first, j = last, p = vector[(first + last) / 2];
    do
    {
        while (vector[i] < p) i++;
        while (p < vector[j]) j--;
        if (i <= j)
        {
            if (i < j) std::swap(vector[i], vector[j]);
            i++; j--;
        }
    } while (i <= j);

    if (i < last) qicksort(vector, i, last);
    if (first < j) qicksort(vector, first, j);
}

int main()
{
    std::vector<int> vector = {3, 5, 5, 2, 9, 1, 66, 3, 2, 6, 8, 13, 5, 1};
    qicksort(vector, 0, vector.size() - 1);
    for (int el : vector) std::cout << el << " ";
    
    return 0;
}
