#include <iostream>
#include <algorithm>

inline bool isLowerBitSet(unsigned number)
{
    return number & 1;
}

unsigned maxBinaryGup(unsigned number)
{
    unsigned counter = 0;
    unsigned max = 0;
    
    while (not isLowerBitSet(number))
        number >>= 1;
    while (number)
    {
        counter = isLowerBitSet(number) ? 0 : counter + 1;
        max = std::max(max, counter);
        number >>= 1;
    }

    return max;
}

int main()
{
    std::cout << "max binary gup: " << maxBinaryGup(9) << std::endl;
    std::cout << "max binary gup: " << maxBinaryGup(37) << std::endl;

    return 0;
}
