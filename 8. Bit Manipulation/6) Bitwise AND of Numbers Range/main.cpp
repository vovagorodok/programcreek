#include <iostream>

unsigned bitwiseAndOfNumbersRange(unsigned m, unsigned n)
{
    while (n > m)
        n = n & n - 1;
    return n & m;
}

int main()
{
    std::cout << "bitwise and: " << bitwiseAndOfNumbersRange(5, 7) << std::endl;

    return 0;
}
