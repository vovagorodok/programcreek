#include <iostream>

unsigned reverseBits(unsigned number)
{
    constexpr unsigned bitsNumber = sizeof(unsigned) * 8;
    unsigned reversedNumber = 0;
    
    for (unsigned i = 0; i < bitsNumber; i++)
    {
        reversedNumber <<= 1;
        reversedNumber |= number & 1;
        number >>= 1;
    }

    return reversedNumber;
}

int main()
{
    std::cout << "reversed bits: " << reverseBits(43261596) << std::endl;
    std::cout << "reversed bits: " << reverseBits(2147483648) << std::endl;

    return 0;
}
