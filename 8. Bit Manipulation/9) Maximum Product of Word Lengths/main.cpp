#include <iostream>
#include <vector>
#include <string_view>
#include <algorithm>

unsigned hash(std::string_view word)
{
    unsigned res = 0;
    for (auto ch : word)
        res |= 1 << (ch - 'a');
    return res;
}

unsigned maximumProductOfWordLengths(std::vector<std::string_view> words)
{
    unsigned max = 0;
    std::vector<unsigned> hashes(words.size());
    
    for (unsigned i = 0; i < words.size(); i++)
        hashes[i] = hash(words[i]);
        
    for (unsigned i = 0; i < words.size(); i++)
        for (unsigned j = i + 1; j < words.size(); j++)
            if (not (hashes[i] & hashes[j]))
                max = std::max(max, unsigned(words[i].size() * words[j].size()));

    return max;
}

int main()
{
    std::cout << "maximum product of word lengths: " << maximumProductOfWordLengths(
        {"abcw", "baz", "foo", "bar", "xtfn", "abcdef"}) << std::endl;
    std::cout << "maximum product of word lengths: " << maximumProductOfWordLengths(
        {"a", "ab", "abc", "d", "cd", "bcd", "abcd"}) << std::endl;
    std::cout << "maximum product of word lengths: " << maximumProductOfWordLengths(
        {"a", "aa", "aaa", "aaaa"}) << std::endl;
           
    return 0;
}
