#include <iostream>
#include <list>

unsigned binaryToGray(unsigned num)
{
    return num ^ (num >> 1);
}

std::list<unsigned> genGrayCode(unsigned n)
{
    std::list<unsigned> ret{};
    for (unsigned i = 0; i <= n; i++)
        ret.push_back(binaryToGray(i));
    return ret;
}

int main()
{
    std::cout << "gray codes:" << std::endl;
    for (unsigned gray : genGrayCode(10))
        std::cout << gray << " ";

    return 0;
}
