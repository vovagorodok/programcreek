#include <iostream>
#include <vector>

std::vector<unsigned> countBits(unsigned n)
{
    std::vector<unsigned> counters(n + 1);
    
    unsigned powBefore = 1;
    unsigned pow = 1;
    for (unsigned i = 1; i <= n; i++)
        if (i == pow)
        {
            counters[i] = 1;
            powBefore = pow;
            pow <<= 1;
        }
        else
        {
            counters[i] = counters[i & ~powBefore] + 1;
        }

    return counters;
}

int main()
{
    std::cout << "counted bits:" << std::endl;
    for (unsigned bitsNumber : countBits(5))
        std::cout << bitsNumber << " ";

    return 0;
}
