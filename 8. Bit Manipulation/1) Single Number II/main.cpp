#include <iostream>
#include <list>

int singleNumber2(std::list<int> numbers)
{
    int ones = 0, twos = 0, threes = 0;
    for (unsigned num : numbers)
    {
        twos |= ones & num;
        ones ^= num;
        threes = ones & twos;
        ones &= ~threes;
        twos &= ~threes;
    }
    return ones;
}

int main()
{
    std::cout << "single number: "
              << singleNumber2({99, 1, 5, 1, 1, 6, 99, 5, 5, 99}) << std::endl;

    return 0;
}
