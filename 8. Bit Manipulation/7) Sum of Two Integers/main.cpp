#include <iostream>

unsigned sumOfTwoIntegers(unsigned a, unsigned b)
{
    unsigned carry = 0;
    while (b)
    {
        carry = a & b;
        a ^= b;
        b = carry << 1;
    }
    return a;
}

int main()
{
    std::cout << "bitwise and: " << sumOfTwoIntegers(5, 7) << std::endl;

    return 0;
}
