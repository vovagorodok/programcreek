#include <iostream>

unsigned numberOf1Bits(unsigned number)
{
    unsigned counter = 0;
    for (; number; number >>= 1)
        if (number & 1) counter++;
    return counter;
}

int main()
{
    std::cout << "number of 1 bits: " << numberOf1Bits(9) << std::endl;
    std::cout << "number of 1 bits: " << numberOf1Bits(37) << std::endl;

    return 0;
}
