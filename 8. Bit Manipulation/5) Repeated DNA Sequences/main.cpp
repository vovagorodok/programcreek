#include <iostream>
#include <string_view>
#include <map>
#include <list>
#include <set>

const std::map<std::string_view::value_type, unsigned> map =
    {{'A', 0}, {'C', 1}, {'G', 2}, {'T', 3}};
constexpr unsigned sequenceSize = 10;
constexpr unsigned simbolSize = 2;
constexpr unsigned sequenceMask = (1 << (sequenceSize * simbolSize)) - 1;
constexpr unsigned lastSimbolAtSequence = sequenceSize - 1;

std::list<std::string_view> repeatedDnaSequences(std::string_view dna)
{
    std::set<unsigned> sequencesUsedOnce{};
    std::set<unsigned> sequencesRepeated{};
    std::list<std::string_view> result{};
    
    unsigned hash = 0;
    for (unsigned i = 0; i < dna.size(); i++)
    {
        hash = ((hash << simbolSize) | map.at(dna[i])) & sequenceMask;
        if (i >= lastSimbolAtSequence)
        {
            if (not sequencesUsedOnce.count(hash))
            {
                sequencesUsedOnce.insert(hash);
            }
            else if (not sequencesRepeated.count(hash))
            {
                sequencesRepeated.insert(hash);
                result.push_back(dna.substr(i - lastSimbolAtSequence, sequenceSize));
            }
        }
    }

    return result;
}

int main()
{
    std::cout << "repeated DNA sequences:" << std::endl;
    for (std::string_view sequence : repeatedDnaSequences("AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT"))
        std::cout << sequence << " ";

    return 0;
}
