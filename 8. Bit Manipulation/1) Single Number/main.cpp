#include <iostream>
#include <list>

unsigned singleNumber(std::list<unsigned> numbers)
{
    unsigned ret = 0;
    for (unsigned num : numbers)
        ret ^= num;
    return ret;
}

int main()
{
    std::cout << "single number: "
              << singleNumber({1, 5, 1, 6, 99, 5, 99}) << std::endl;

    return 0;
}
