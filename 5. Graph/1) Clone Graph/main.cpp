#include <iostream>
#include <memory>
#include <set>
#include <map>

struct Node
{
    int data;
    std::set<std::shared_ptr<Node>> nodes;
    Node(int data) : data(data), nodes() {}
};

void connect(std::shared_ptr<Node> node1, std::shared_ptr<Node> node2)
{
    node1->nodes.insert(node2);
    node2->nodes.insert(node1);
}

void print(std::shared_ptr<Node> node, std::set<std::shared_ptr<Node>>& visited)
{
    std::cout << node->data << " -> { ";
    visited.insert(node);
    for (auto neighbor : node->nodes)
        std::cout << neighbor->data << " ";
    std::cout << "}" << std::endl;
    for (auto neighbor : node->nodes)
        if (not visited.count(neighbor))
            print(neighbor, visited);
}

void print(std::shared_ptr<Node> node)
{
    std::set<std::shared_ptr<Node>> visited{};
    print(node, visited);
}

void copy(std::shared_ptr<Node> node,
          std::map<std::shared_ptr<Node>, std::shared_ptr<Node>>& copied)
{
    copied[node] = std::make_shared<Node>(node->data);
    for (auto neighbor : node->nodes)
    {
        if (not copied.count(neighbor))
            copy(neighbor, copied);
        connect(copied[node], copied[neighbor]);
    }
}

std::shared_ptr<Node> copy(std::shared_ptr<Node> node)
{
    std::map<std::shared_ptr<Node>, std::shared_ptr<Node>> copied{};
    copy(node, copied);
    return copied[node];
}

int main()
{ 
    auto node1 = std::make_shared<Node>(1);
    auto node2 = std::make_shared<Node>(2);
    auto node3 = std::make_shared<Node>(3);
    auto node4 = std::make_shared<Node>(4);
    connect(node1, node2);
    connect(node1, node3);
    connect(node2, node3);
    connect(node2, node4);
    connect(node3, node4);
    
    std::cout << "original:" << std::endl;
    print(node1);
    std::cout << "copy:" << std::endl;
    print(copy(node1));

    return 0; 
}
