#include <iostream>
#include <string_view>
#include <list>
#include <map>

struct Ticket
{
    std::string_view from;
    std::string_view to;
};

std::list<std::string_view> reconstructItinerary(std::string_view begin, std::list<Ticket> tickets)
{
    std::multimap<std::string_view, std::string_view> multimap{};
    std::list<std::string_view> itinerary{begin};

    for (Ticket ticket : tickets)
        multimap.insert(std::make_pair(ticket.from, ticket.to));
    
    while (not multimap.empty())
    {
        auto it = multimap.find(begin);
        if (it == multimap.end()) break;
        
        begin = it->second;
        itinerary.push_back(begin);
        multimap.erase(it);
    }
    
    return itinerary;
}

int main()
{
    for (auto point : reconstructItinerary("JFK", {{"JFK","SFO"},
                                                   {"JFK","ATL"},
                                                   {"SFO","ATL"},
                                                   {"ATL","JFK"},
                                                   {"ATL", "SFO"}}))
        std::cout << "'" << point << "' ";
    std::cout << std::endl;
    
    return 0;
}
