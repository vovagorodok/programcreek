#include <iostream>
#include <memory>
#include <set>
#include <list>

struct Prerequire
{
    unsigned require;
    unsigned take;
};

bool canFinich(const std::list<Prerequire>& prerequires)
{
    std::set<unsigned> requires{};
    std::set<unsigned> takes{};
    
    for (Prerequire prerequire : prerequires)
        if (requires.count(prerequire.take) and takes.count(prerequire.require))
            return false;
        else
        {
            requires.insert(prerequire.require);
            takes.insert(prerequire.take);
        }
    return true;
}

int main()
{
    std::list<Prerequire> prerequires =
        {{0, 1}, {1, 0}};
    
    std::cout << "can finich: " << std::boolalpha << canFinich(prerequires) << std::endl;

    return 0; 
}
