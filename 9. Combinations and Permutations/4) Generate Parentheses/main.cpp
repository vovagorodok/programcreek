#include <iostream>
#include <string>

void printParenthesis(std::string& str, unsigned pos, unsigned n, unsigned open, unsigned close)
{
    if (close == n)
        std::cout << str << std::endl;
    else
    {
        if (open > close)
        {
            str[pos] = ')';
            printParenthesis(str, pos + 1, n, open, close + 1);
        }
        if (open < n)
        {
            str[pos] = '(';
            printParenthesis(str, pos + 1, n, open + 1, close);
        }
    }
}

void printParenthesis(unsigned n)
{
    std::string str(n * 2, ' ');
    printParenthesis(str, 0, n, 0, 0);
}

int main()
{
    std::cout << "parenthesis permutations:" << std::endl;
    printParenthesis(3);
    
    return 0;
}
