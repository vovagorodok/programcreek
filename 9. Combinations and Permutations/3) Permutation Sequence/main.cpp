#include <iostream>
#include <vector>
#include <cmath>

std::vector<unsigned> makeIncrementedVector(unsigned n)
{
    std::vector<unsigned> vector(n);
    for (unsigned i = 0; i < n; i++)
        vector[i] = i + 1;
    return vector;
}

std::vector<unsigned> getPermutation(unsigned n, unsigned k)
{
    std::vector<unsigned> result{};
    std::vector<unsigned> vector = makeIncrementedVector(n);
    k--;
    unsigned factorial = std::tgamma(n + 1);
    unsigned index;
    
    for (unsigned i = 0; i < n; i++)
    {
        factorial /= (n - i);
        index = k / factorial;
        k %= factorial;
        
        result.push_back(vector.at(index));
        vector.erase(vector.begin() + index);
    }
    
    return result;
}

int main()
{
    std::cout << "permutation: ";
    for (unsigned num : getPermutation(3, 4))
        std::cout << num << " ";
    
    return 0;
}
