#include <iostream>
#include <vector>

void printPermutation(std::vector<int>& vector)
{
    for (int num : vector)
        std::cout << num << " ";
    std::cout << std::endl;
}

void printPermutations(std::vector<int>& vector, unsigned begin)
{
    if (begin == vector.size() - 1)
        printPermutation(vector);
    else
        for (unsigned i = begin; i < vector.size(); i++)
        {
            std::swap(vector[begin], vector[i]);
            printPermutations(vector, begin + 1);
            std::swap(vector[begin], vector[i]);
        }
}

void printPermutations(std::vector<int> vector)
{
    printPermutations(vector, 0);
}

int main()
{
    std::cout << "permutations:" << std::endl;
    printPermutations({1, 2, 3});
    
    return 0;
}
