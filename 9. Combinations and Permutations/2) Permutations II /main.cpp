#include <iostream>
#include <vector>
#include <set>

void printPermutation(std::vector<int>& vector)
{
    for (int num : vector)
        std::cout << num << " ";
    std::cout << std::endl;
}

void printPermutations(std::vector<int>& vector, unsigned begin)
{
    if (begin == vector.size() - 1)
        printPermutation(vector);
    else
    {
        std::set<int> set{};
        for (unsigned i = begin; i < vector.size(); i++)
        {
            if (set.count(vector[i])) continue;
            set.insert(vector[i]);
            
            std::swap(vector[begin], vector[i]);
            printPermutations(vector, begin + 1);
            std::swap(vector[begin], vector[i]);
        }
    }
}

void printPermutations(std::vector<int> vector)
{
    printPermutations(vector, 0);
}

int main()
{
    std::cout << "permutations:" << std::endl;
    printPermutations({1, 1, 2});
    
    return 0;
}
