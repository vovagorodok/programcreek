#include <iostream>
#include <list>

void printList(std::list<unsigned>& list)
{
    for (unsigned num : list)
        std::cout << num << " ";
    std::cout << std::endl;
}

void combinationFactor(unsigned n, std::list<unsigned>& list, unsigned num, unsigned factor)
{
    for (unsigned i = num; i < n; i++)
    {
        factor *= i;
        list.push_back(i);
        
        if (factor == n)
            printList(list);
        else if (factor < n)
            combinationFactor(n, list, i, factor);
        
        factor /= i;
        list.pop_back();
    }
}

void combinationFactor(unsigned n)
{
    std::list<unsigned> list{};
    combinationFactor(n, list, 2, 1);
}

int main()
{
    std::cout << "combination factor:" << std::endl;
    combinationFactor(8);
    
    return 0;
}
