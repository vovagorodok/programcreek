#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <string_view>

const std::map<unsigned, std::string_view> map = {
    {2, "abc"},
    {3, "def"},
    {4, "ghi"},
    {5, "jkl"},
    {6, "mno"},
    {7, "pqrs"},
    {8, "tuv"},
    {9, "wxyz"}
};

void letterCombinationsOfAPhoneNumber(std::vector<unsigned>& numbers, std::string& str, unsigned pos)
{
    if (pos == numbers.size())
    {
        std::cout << str << std::endl;
        return;
    }

    for (auto ch : map.at(numbers[pos]))
    {
        str[pos] = ch;
        letterCombinationsOfAPhoneNumber(numbers, str, pos + 1);
    }
}

void letterCombinationsOfAPhoneNumber(std::vector<unsigned> numbers)
{
    std::string str(numbers.size(), ' ');
    letterCombinationsOfAPhoneNumber(numbers, str, 0);
}

int main()
{
    std::cout << "letter combinations of a phone number:" << std::endl;
    letterCombinationsOfAPhoneNumber({2, 3});
    
    return 0;
}
