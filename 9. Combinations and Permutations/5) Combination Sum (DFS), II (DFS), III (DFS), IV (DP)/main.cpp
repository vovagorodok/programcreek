#include <iostream>
#include <vector>
#include <list>

void printList(std::list<unsigned>& list)
{
    for (unsigned num : list)
        std::cout << num << " ";
    std::cout << std::endl;
}

void combinationSum(std::vector<unsigned>& set, unsigned target,
                    std::list<unsigned>& list, unsigned pos, unsigned sum)
{
    for (unsigned i = pos; i < set.size(); i++)
    {
        sum += set[i];
        list.push_back(set[i]);
        
        if (sum == target)
            printList(list);
        else if (sum < target)
            combinationSum(set, target, list, i, sum);
        
        sum -= set[i];
        list.pop_back();
    }
}

void combinationSum(std::vector<unsigned> set, unsigned target)
{
    std::list<unsigned> list{};
    combinationSum(set, target, list, 0, 0);
}

void combinationSum2(std::vector<unsigned>& set, unsigned target,
                    std::list<unsigned>& list, unsigned pos, unsigned sum)
{
    for (unsigned i = pos; i < set.size(); i++)
    {
        sum += set[i];
        list.push_back(set[i]);
        
        if (sum == target)
            printList(list);
        else if (sum < target)
            combinationSum2(set, target, list, i + 1, sum);
        
        sum -= set[i];
        list.pop_back();
    }
}

void combinationSum2(std::vector<unsigned> set, unsigned target)
{
    std::list<unsigned> list{};
    combinationSum2(set, target, list, 0, 0);
}

void combinationSum3(unsigned k, unsigned n,
                     std::list<unsigned>& list, unsigned num, unsigned sum)
{
    if (k == 0)
    {
        if (sum == n) printList(list);
        return;
    }
    
    for (unsigned i = num; i <= n; i++)
    {
        sum += i;
        list.push_back(i);
        
        combinationSum3(k - 1, n, list, i + 1, sum);
        
        sum -= i;
        list.pop_back();
    }
}

void combinationSum3(unsigned k, unsigned n)
{
    std::list<unsigned> list{};
    combinationSum3(k, n, list, 1, 0);
}

unsigned combinationSum4(std::vector<unsigned> set, unsigned target)
{
    unsigned dp[target + 1]{};
    dp[0] = 1;
    
    for (unsigned i = 0; i <= target; i++)
        for (unsigned num : set)
            if (i + num <= target)
                dp[i + num] += dp[i];
                
    return dp[target];
}

int main()
{
    std::cout << "combination sum:" << std::endl;
    combinationSum({2, 3, 6, 7}, 7);
    
    std::cout << "combination sum 2:" << std::endl;
    combinationSum2({2, 3, 6, 7}, 7);
    
    std::cout << "combination sum 3:" << std::endl;
    combinationSum3(3, 7);
    std::cout << "combination sum 3:" << std::endl;
    combinationSum3(3, 9);
    
    std::cout << "combination sum 4: "
              << combinationSum4({1, 2, 3}, 4);
    
    return 0;
}
