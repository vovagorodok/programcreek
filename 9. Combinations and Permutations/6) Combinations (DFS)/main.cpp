#include <iostream>
#include <list>

void printList(std::list<unsigned>& list)
{
    for (unsigned num : list)
        std::cout << num << " ";
    std::cout << std::endl;
}

void combinations(unsigned k, unsigned n, std::list<unsigned>& list, unsigned num)
{
    if (k == 0)
    {
        printList(list);
        return;
    }
    
    for (unsigned i = num; i <= n; i++)
    {
        list.push_back(i);
        combinations(k - 1, n, list, i + 1);
        list.pop_back();
    }
}

void combinations(unsigned k, unsigned n)
{
    std::list<unsigned> list{};
    combinations(k, n, list, 1);
}

int main()
{
    std::cout << "combinations:" << std::endl;
    combinations(2, 4);
    
    return 0;
}
