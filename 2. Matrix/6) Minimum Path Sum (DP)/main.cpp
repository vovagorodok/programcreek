#include <iostream>
#include <vector>

using Matrix = std::vector<std::vector<int>>;

int minPathSum(Matrix& matrix)
{
    const unsigned rows = matrix.size();
    const unsigned cols = matrix.front().size();
    
    int array[cols];
    
    array[0] = matrix[0][0];

    for (unsigned col = 1; col < cols; col++)
        array[col] = matrix[0][col] + array[col - 1];
        
    for (unsigned row = 1; row < rows; row++)
        for (unsigned col = 1; col < cols; col++)
            array[col] = matrix[row][col] + std::min(array[col], array[col - 1]);
    
    return array[cols - 1];
}

int main()
{
    Matrix matrix =
        {{0, 1, 0, 0, 0},
         {0, 1, 0, 1, 0},
         {0, 1, 0, 1, 0},
         {0, 1, 0, 1, 0},
         {0, 0, 0, 1, 0}};
    std::cout << "min path sum: " << minPathSum(matrix) << std::endl;

    return 0;
}
