#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

using Matrix = std::vector<std::vector<unsigned>>;

unsigned minTotalDistance(const Matrix& map)
{
    unsigned distance = 0;
    std::vector<unsigned> rows{}, cols{};

    for (unsigned row = 0; row < map.size(); row++)
        for (unsigned col = 0; col < map.front().size(); col++)
            if (map[row][col] == 1)
            {
                rows.push_back(row);
                cols.push_back(col);
            }
    
    std::sort(rows.begin(), rows.end());
    std::sort(cols.begin(), cols.end());
    
    unsigned bestRow = rows[rows.size() / 2];
    unsigned bestCol = cols[cols.size() / 2];
    
    for (unsigned row : rows) distance += std::abs(int(bestRow - row));
    for (unsigned col : cols) distance += std::abs(int(bestCol - col));

    return distance;
}

int main()
{
    Matrix matrix =
        {{1, 0, 0, 0, 1},
         {0, 0, 0, 0, 0},
         {0, 0, 1, 0, 0}};

    std::cout << "min total distance: " << minTotalDistance(matrix) << std::endl;

    return 0;
}
