#include <iostream>
#include <vector>

using Matrix = std::vector<std::vector<int>>;

void print(const Matrix& matrix)
{
    for (auto& row : matrix)
    {
        for (int el : row)
            std::cout << el << " ";
        std::cout << std::endl;
    }
}

inline bool hasRowZero(const Matrix& matrix, unsigned row,  unsigned col,  unsigned cols)
{
    for (unsigned i = col; i < cols; i++)
        if (matrix[row][i] == 0) return true;
    return false;
}

inline bool hasColZero(const Matrix& matrix, unsigned row,  unsigned col,  unsigned rows)
{
    for (unsigned i = row; i < rows; i++)
        if (matrix[i][col] == 0) return true;
    return false;
}

inline void fillRowByZeros(Matrix& matrix, unsigned row, unsigned cols)
{
    for (unsigned i = 0; i < cols; i++) matrix[row][i] = 0;
}

inline void fillColByZeros(Matrix& matrix, unsigned col, unsigned rows)
{
    for (unsigned i = 0; i < rows; i++) matrix[i][col] = 0;
}

void setMatrixZeroes(Matrix& matrix)
{
    const unsigned rows = matrix.size();
    const unsigned cols = matrix.front().size();
    
    for (unsigned i = 0; i < rows or i < cols; i++)
    {
        bool zeroInRow = false;
        bool zeroInCol = false;
        
        if (i >= rows)
            zeroInCol = hasColZero(matrix, rows - 1, i, rows);
        else if (i >= cols)
            zeroInRow = hasRowZero(matrix, i, cols - 1, cols);
        else
        {
            zeroInRow = hasRowZero(matrix, i, i, cols);
            zeroInCol = hasColZero(matrix, i, i, rows);
        }

        if (zeroInRow) fillRowByZeros(matrix, i, cols);
        if (zeroInCol) fillColByZeros(matrix, i, rows);
    }
}

int main()
{
    Matrix matrix =
        {{1, 0, 1, 1, 0, 1},
         {0, 1, 1, 1, 0, 1},
         {1, 1, 1, 1, 1, 1},
         {1, 0, 1, 1, 1, 1},
         {1, 1, 1, 1, 1, 0}};
    
    std::cout << "matrix before:" << std::endl;
    print(matrix);
    setMatrixZeroes(matrix);
    std::cout << "matrix after:" << std::endl;
    print(matrix);

    return 0;
}
