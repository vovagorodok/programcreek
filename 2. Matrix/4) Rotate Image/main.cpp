#include <iostream>
#include <iomanip>
#include <vector>

using Matrix = std::vector<std::vector<int>>;

void print(const Matrix& matrix)
{
    for (auto& row : matrix)
    {
        for (int el : row)
            std::cout << std::setfill('0') << std::setw(2) << el << " ";
        std::cout << std::endl;
    }
}

void rotate(Matrix& matrix)
{
    const unsigned size = matrix.size();
    const unsigned last = size - 1;
    
    for (unsigned col = 0; col < last / 2; col++)
        for (unsigned row = 0; row < (size + 1) / 2; row++)
        {
            std::swap(matrix[row][col], matrix[last - col][row]);
            std::swap(matrix[last - col][row], matrix[last - row][last - col]);
            std::swap(matrix[last - row][last - col], matrix[col][last - row]);
        }
}

int main()
{
    Matrix matrix =
        {{00, 01, 02, 03, 04},
         {10, 11, 12, 13, 14},
         {20, 21, 22, 23, 24},
         {30, 31, 32, 33, 34},
         {40, 41, 42, 43, 44}};
    
    std::cout << "matrix before:" << std::endl;
    print(matrix);
    rotate(matrix);
    std::cout << "matrix after:" << std::endl;
    print(matrix);

    return 0;
}
