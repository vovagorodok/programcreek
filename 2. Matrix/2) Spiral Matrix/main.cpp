#include <iostream>
#include <vector>

using Matrix = std::vector<std::vector<int>>;

void prntSpiralOrder(const Matrix& matrix)
{
    int down = 0;
    int up = matrix.size() - 1;
    int left = 0;
    int right = matrix.front().size() - 1;
    
    while (down <= up and left <= right)
    {
        for (int i = left; i <= right; i++)
            std::cout << matrix[down][i] << " ";
        down++;
            
        for (int i = down; i <= up; i++)
            std::cout << matrix[i][right] << " ";
        right--;
        
        for (int i = right; i >= left; i--)
            std::cout << matrix[up][i] << " ";
        up--;
        
        for (int i = up; i >= down; i--)
            std::cout << matrix[i][left] << " ";
        left++;
    }
}

int main()
{
    Matrix matrix =
        {{0, 1, 2, 3, 4},
         {5, 6, 7, 8, 9},
         {0, 1, 2, 3, 4},
         {5, 6, 7, 8, 9},
         {0, 1, 2, 3, 4}};
    
    prntSpiralOrder(matrix);

    return 0;
}
