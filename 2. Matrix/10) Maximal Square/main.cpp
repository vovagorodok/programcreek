#include <iostream>
#include <vector>
#include <algorithm>

using Matrix = std::vector<std::vector<unsigned>>;

unsigned findMaxSquare(Matrix& matrix)
{
    const unsigned rows = matrix.size();
    const unsigned cols = matrix.front().size();
    
    unsigned maxSquare = 0;
    unsigned array[rows][cols];
    
    for (unsigned col = 0; col < cols; col++) array[0][col] = matrix[0][col];
    for (unsigned row = 0; row < rows; row++) array[row][0] = matrix[row][0];
    
    for (unsigned row = 1; row < rows; row++)
        for (unsigned col = 1; col < cols; col++)
            if (matrix[row][col])
            {
                unsigned min = std::min(array[row - 1][col - 1],
                               std::min(array[row - 1][col], array[row][col - 1]));
                array[row][col] = min + 1;
                
                maxSquare = std::max(maxSquare, array[row][col]);
            }
            else array[row][col] = 0;
    
    return maxSquare * maxSquare;
}

int main()
{
    Matrix matrix =
        {{1, 1, 1, 0, 1},
         {1, 1, 1, 0, 1},
         {1, 1, 1, 0, 1},
         {1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1}};
    std::cout << "max square: " << findMaxSquare(matrix) << std::endl;

    return 0;
}
