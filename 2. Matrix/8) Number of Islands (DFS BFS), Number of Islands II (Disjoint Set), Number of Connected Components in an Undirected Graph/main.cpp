#include <iostream>
#include <list>
#include <utility>

using Edge = std::pair<unsigned, unsigned>;
using ListOfEdges = std::list<Edge>;

unsigned countComponents(unsigned n, const ListOfEdges& edges)
{
    unsigned nodes[n];
    for (unsigned i = 0; i < n; i++) nodes[i] = i;
    
    auto findRoot = [&nodes](unsigned node)
    {
        while (nodes[node] != node)
        {
            nodes[node] = nodes[nodes[node]];
            node = nodes[node];
        }
        return node;
    };
    
    for (auto edge : edges)
    {
        unsigned root1 = findRoot(edge.first);
        unsigned root2 = findRoot(edge.second);
        
        if (root1 != root2)
        {
            n--;
            nodes[root1] = root2;
        }
    }
    
    return n;
}

int main()
{
    ListOfEdges edges = {{0, 1}, {1, 2}, {3, 4}};
    
    std::cout << "number of connected components: "
              << countComponents(5, edges) << std::endl;

    return 0;
}
