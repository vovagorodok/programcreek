#include <iostream>
#include <iomanip>
#include <vector>

using Matrix = std::vector<std::vector<unsigned>>;

void print(const Matrix& matrix)
{
    for (auto& row : matrix)
    {
        for (int el : row)
            std::cout << std::setw(3) << el << " ";
        std::cout << std::endl;
    }
}

void prepareSummedAreaTable(Matrix& matrix)
{
    const unsigned rows = matrix.size();
    const unsigned cols = matrix.front().size();

    for (unsigned col = 1; col < cols; col++) matrix[0][col] += matrix[0][col - 1];
    for (unsigned row = 1; row < rows; row++) matrix[row][0] += matrix[row - 1][0];
    for (unsigned row = 1; row < rows; row++)
        for (unsigned col = 1; col < cols; col++)
            matrix[row][col] += matrix[row][col - 1] + matrix[row - 1][col] - matrix[row - 1][col - 1];
}

unsigned calcSumForRectangle(Matrix& matrix, unsigned startRow, unsigned startCol, unsigned endRow, unsigned endCol)
{
    prepareSummedAreaTable(matrix);
    
    if (startRow == 0 and startCol == 0)
        return matrix[endRow][endCol];
    else if (startRow == 0)
        return matrix[endRow][endCol] - matrix[endRow][startCol - 1];
    else if (startCol == 0)
        return matrix[endRow][endCol] - matrix[startRow - 1][endCol];
    else
        return matrix[endRow][endCol] + matrix[startRow - 1][startCol - 1] -
               matrix[endRow][startCol - 1] - matrix[startRow - 1][endCol];
}

int main()
{
    Matrix matrix =
        {{1, 1, 1, 0, 1},
         {1, 1, 1, 0, 1},
         {1, 1, 1, 0, 1},
         {1, 1, 4, 1, 1},
         {1, 1, 1, 1, 1}};
    std::cout << "max square: " << calcSumForRectangle(matrix, 1, 1, 4, 4) << std::endl;
    std::cout << "summed area table:" << std::endl;
    print(matrix);

    return 0;
}
