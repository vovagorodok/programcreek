#include <iostream>
#include <vector>

using Matrix = std::vector<std::vector<int>>;

bool searchMatrix(const Matrix& matrix, int target)
{
    const int rows = matrix.size() - 1;
    const int cols = matrix.front().size() - 1;
    
    int row = rows;
    int col = 0;
    
    while (row >= 0 and col <= cols)
    {
        if (matrix[row][col] > target)
            row--;
        else if (matrix[row][col] < target)
            col++;
        else
            return true;
    }
    
    return false;
}

int main()
{
    const int target = 5;
    const Matrix matrix =
        {{ 1,  4,  7, 11, 15},
         { 2,  5,  8, 12, 19},
         { 3,  6,  9, 16, 22},
         {10, 13, 14, 17, 24},
         {18, 21, 23, 26, 30}};
    
    std::cout << "matrix contains " << target << ": "
              << std::boolalpha << searchMatrix(matrix, target) << std::endl;

    return 0;
}
