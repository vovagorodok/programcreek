#include <iostream>
#include <vector>
#include <stack>
#include <algorithm>

using Matrix = std::vector<std::vector<int>>;

int maxAreaInHistogram(const std::vector<int>& histogram)
{
    std::stack<int> stack{};
    int maxArea = 0;
    int i = 0;
    
    while (i < histogram.size())
    {
        if (stack.empty() or histogram[stack.top()] <= histogram[i])
        {
            stack.push(i);
            i++;
        }
        else
        {
            const int width = i - stack.top();
            const int high = histogram[stack.top()];
            const int area = width * high;
            maxArea = std::max(maxArea, area);
            stack.pop();
        }
    }
    
    while (!stack.empty())
    {
        const int width = i - stack.top();
        const int high = histogram[stack.top()];
        const int area = width * high;
        maxArea = std::max(maxArea, area);
        stack.pop();
    }
 
    return maxArea;
}

void prepare(Matrix& matrix)
{
    const int rows = matrix.size();
    const int cols = matrix.front().size();
    
    for (int row = 0; row < rows; row++)
        for (int col = 0; col < cols; col++)
        {
            if (matrix[row][col] and row)
                matrix[row][col] = matrix[row - 1][col] + 1;
        }
}

int maxRectangle(Matrix& matrix)
{
    int maxArea = 0;
    
    prepare(matrix);
    for (auto& row : matrix)
        maxArea = std::max(maxArea, maxAreaInHistogram(row));

    return maxArea;
}


int main()
{
    Matrix matrix =
        {{0, 0, 0, 0, 0},
         {0, 1, 1, 1, 0},
         {0, 1, 1, 1, 0},
         {0, 1, 0, 1, 0},
         {0, 0, 0, 1, 0}};

    std::cout << "max rectangle area: " << maxRectangle(matrix) << std::endl;

    return 0;
}
