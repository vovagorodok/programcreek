#include <iostream>

int uniquePaths(unsigned rows, unsigned cols)
{
    int array[cols];

    for (unsigned col = 0; col < cols; col++)
        array[col] = 1;
        
    for (unsigned row = 1; row < rows; row++)
        for (unsigned col = 1; col < cols; col++)
            array[col] = array[col] + array[col - 1];
    
    return array[cols - 1];
}

int main()
{
    std::cout << "unique paths: " << uniquePaths(1, 1) << std::endl;
    std::cout << "unique paths: " << uniquePaths(1, 2) << std::endl;
    std::cout << "unique paths: " << uniquePaths(2, 2) << std::endl;
    std::cout << "unique paths: " << uniquePaths(2, 3) << std::endl;
    std::cout << "unique paths: " << uniquePaths(3, 3) << std::endl;
    std::cout << "unique paths: " << uniquePaths(4, 3) << std::endl;
    std::cout << "unique paths: " << uniquePaths(4, 4) << std::endl;
    std::cout << "unique paths: " << uniquePaths(5, 4) << std::endl;
    std::cout << "unique paths: " << uniquePaths(5, 5) << std::endl;
    std::cout << "unique paths: " << uniquePaths(6, 5) << std::endl;
    std::cout << "unique paths: " << uniquePaths(6, 6) << std::endl;
    std::cout << "unique paths: " << uniquePaths(2, 4) << std::endl;

    return 0;
}
