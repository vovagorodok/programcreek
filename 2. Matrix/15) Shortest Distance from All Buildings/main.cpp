#include <iostream>
#include <iomanip>
#include <vector>
#include <queue>
#include <limits>
#include <algorithm>

using Matrix = std::vector<std::vector<unsigned>>;
constexpr unsigned OBSTACLE = std::numeric_limits<unsigned>::max();
constexpr unsigned BUILDING = OBSTACLE - 1;
constexpr unsigned SPACE = 0;
constexpr unsigned o = OBSTACLE;
constexpr unsigned b = BUILDING;

struct Coordinate { unsigned row, col; };
using CoordinatesQueue = std::queue<Coordinate>;

void print(const Matrix& matrix)
{
    for (auto& row : matrix)
    {
        for (int el : row)
            if (el == OBSTACLE) std::cout << std::setw(2) << 'o' << " ";
            else if (el == BUILDING) std::cout << std::setw(2) << 'b' << " ";
            else if (el == SPACE) std::cout << std::setw(2) << '.' << " ";
            else std::cout << std::setw(2) << el << " ";
        std::cout << std::endl;
    }
}

void initializeWavePropagationMap(Matrix& wavePropagationMap, const Matrix& map)
{
    for (unsigned row = 0; row < map.size(); row++)
        for (unsigned col = 0; col < map.front().size(); col++)
            wavePropagationMap[row][col] = map[row][col];
}

void evaluateNeighbour(Matrix& wavePropagationMap, CoordinatesQueue& queue, Coordinate neighbour)
{
    if (wavePropagationMap[neighbour.row][neighbour.col] == SPACE)
    {
        wavePropagationMap[neighbour.row][neighbour.col] = wavePropagationMap[queue.front().row][queue.front().col] + 1;
        queue.push(neighbour);
    }
    else if (wavePropagationMap[neighbour.row][neighbour.col] == BUILDING)
    {
        wavePropagationMap[neighbour.row][neighbour.col] = wavePropagationMap[queue.front().row][queue.front().col] + 1;
    }
}

void evaluateCoordinate(Matrix& wavePropagationMap, CoordinatesQueue& queue)
{
    const unsigned lastRow = wavePropagationMap.size() - 1;
    const unsigned lastCol = wavePropagationMap.front().size() - 1;
    
    Coordinate coord = queue.front();
    
    if (coord.row != 0)
        evaluateNeighbour(wavePropagationMap, queue, {coord.row - 1, coord.col});
    if (coord.col != 0)
        evaluateNeighbour(wavePropagationMap, queue, {coord.row, coord.col - 1});
    if (coord.row != lastRow)
        evaluateNeighbour(wavePropagationMap, queue, {coord.row + 1, coord.col});
    if (coord.col != lastCol)
        evaluateNeighbour(wavePropagationMap, queue, {coord.row, coord.col + 1});
        
    queue.pop();
}

void fillWavePropagationMap(Matrix& wavePropagationMap, Coordinate start)
{
    CoordinatesQueue queue{};
    queue.push(start);

    evaluateCoordinate(wavePropagationMap, queue);
    wavePropagationMap[start.row][start.col] = OBSTACLE;
    
    while (not queue.empty())
        evaluateCoordinate(wavePropagationMap, queue);
}

unsigned calculateDistanceFromAllBuildings(Matrix& wavePropagationMap, const Matrix& map)
{
    unsigned distance = 0;
    
    for (unsigned row = 0; row < map.size(); row++)
        for (unsigned col = 0; col < map.front().size(); col++)
            if (map[row][col] == BUILDING) distance += wavePropagationMap[row][col];
            
    return distance;
}

unsigned findShortestDistanceFromAllBuildings(const Matrix& map)
{
    unsigned shortestDistance = std::numeric_limits<unsigned>::max();
    Coordinate shortestStart{};
    Matrix wavePropagationMap(map.size(), Matrix::value_type(map.front().size()));

    for (unsigned row = 0; row < map.size(); row++)
        for (unsigned col = 0; col < map.front().size(); col++)
            if (map[row][col] == SPACE)
            {
                Coordinate start = {row, col};
                initializeWavePropagationMap(wavePropagationMap, map);
                fillWavePropagationMap(wavePropagationMap, start);
                unsigned distance = calculateDistanceFromAllBuildings(wavePropagationMap, map);
                
                if (shortestDistance > distance)
                {
                    shortestDistance = distance;
                    shortestStart = start;
                }
            }

    std::cout << "(shortest start: " << shortestStart.row << ", " << shortestStart.col << ") ";
    return shortestDistance;
}

int main()
{
    Matrix matrix =
        {{b, 0, 0, 0, 0},
         {0, o, 0, o, b},
         {0, o, 0, 0, 0},
         {0, 0, b, 0, 0},
         {b, 0, 0, 0, o}};
    
    std::cout << "map:" << std::endl;
    print(matrix);
    std::cout << "shortest distance: " << findShortestDistanceFromAllBuildings(matrix) << std::endl;

    return 0;
}
