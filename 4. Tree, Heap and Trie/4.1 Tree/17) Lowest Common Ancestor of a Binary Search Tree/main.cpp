#include <iostream>
#include <memory>

struct Node
{
    int data;
    std::shared_ptr<Node> left, right;
    Node(int data) : data(data), left(nullptr), right(nullptr) {}
};

std::shared_ptr<Node> commonAncestor(std::shared_ptr<Node> ancestor,
                                     std::shared_ptr<Node> node1,
                                     std::shared_ptr<Node> node2)
{
    if (not ancestor)
        return nullptr;
    if (ancestor->data < node1->data and ancestor->data < node2->data)
        return commonAncestor(ancestor->right, node1, node2);
    if (ancestor->data > node1->data and ancestor->data > node2->data)
        return commonAncestor(ancestor->left, node1, node2);
    return ancestor;
}

int main() 
{ 
    auto root = std::make_shared<Node>(20);
    root->left = std::make_shared<Node>(15);
    root->right = std::make_shared<Node>(25);
    root->left->left = std::make_shared<Node>(10);
    root->right->right = std::make_shared<Node>(30);

    std::cout << "common ancesstor: "
              << commonAncestor(root, root->left->left, root->right->right)->data
              << std::endl;

    return 0; 
}
