#include <iostream>
#include <memory>

struct Node
{
    int data;
    std::shared_ptr<Node> left, right;
    Node(int data) : data(data), left(nullptr), right(nullptr) {}
};

std::shared_ptr<Node> commonAncestor(std::shared_ptr<Node> ancestor,
                                     std::shared_ptr<Node> node1,
                                     std::shared_ptr<Node> node2)
{
    if (not ancestor)
        return nullptr;

    if (ancestor->data == node1->data or ancestor->data == node2->data)
        return ancestor;
        
    auto ancestor1 = commonAncestor(ancestor->left, node1, node2);
    auto ancestor2 = commonAncestor(ancestor->right, node1, node2);
    
    if (ancestor1 and ancestor2) return ancestor;
    if (ancestor1) return ancestor1;
    if (ancestor2) return ancestor2;
    return nullptr;
}

int main() 
{ 
    auto root = std::make_shared<Node>(4);
    root->left = std::make_shared<Node>(5);
    root->right = std::make_shared<Node>(7);
    root->left->left = std::make_shared<Node>(2);
    root->right->right = std::make_shared<Node>(1);

    std::cout << "common ancesstor: "
              << commonAncestor(root, root->left->left, root->right->right)->data
              << std::endl;

    return 0; 
}
