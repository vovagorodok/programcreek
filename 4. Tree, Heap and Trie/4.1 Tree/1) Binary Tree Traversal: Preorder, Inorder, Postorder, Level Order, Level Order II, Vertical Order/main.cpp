#include <iostream>
#include <memory>
#include <queue>
#include <stack>
#include <list>

struct Node
{
    int data;
    std::shared_ptr<Node> left, right;
    Node(int data) : data(data), left(nullptr), right(nullptr) {}
}; 

void printPreorderRecursive(std::shared_ptr<Node> node) 
{ 
    if (not node) return;

    std::cout << node->data << " ";
    printPreorderRecursive(node->left);
    printPreorderRecursive(node->right);
}

void printInorderRecursive(std::shared_ptr<Node> node) 
{ 
    if (not node) return; 

    printInorderRecursive(node->left);
    std::cout << node->data << " ";
    printInorderRecursive(node->right);
}

void printPostorderRecursive(std::shared_ptr<Node> node)
{ 
    if (not node)  return;

    printPostorderRecursive(node->left);
    printPostorderRecursive(node->right);
    std::cout << node->data << " ";
}

void printPreorder(std::shared_ptr<Node> node)
{
    if (not node) return;
    
    std::stack<std::shared_ptr<Node>> stack{};
    stack.push(node);
    
    while (not stack.empty())
    {
        auto topNode = stack.top();
        stack.pop();
        
        std::cout << topNode->data << " ";
        
        if (topNode->right) stack.push(topNode->right);
        if (topNode->left) stack.push(topNode->left);
    }
}

void printInorder(std::shared_ptr<Node> node)
{
    if (not node) return;
    
    std::stack<std::shared_ptr<Node>> stack{};
    auto topNode = node;
    while (topNode)
    {
        stack.push(topNode);
        topNode = topNode->left;
    }
    
    while (not stack.empty())
    {
        topNode = stack.top();
        stack.pop();
        
        std::cout << topNode->data << " ";
        
        topNode = topNode->right;
        while (topNode)
        {
            stack.push(topNode);
            topNode = topNode->left;
        }
    }
}

void printPostorder(std::shared_ptr<Node> node)
{
    if (not node) return;
    
    std::stack<std::shared_ptr<Node>> stack{};
    stack.push(node);
    
    while (not stack.empty())
    {
        auto topNode = stack.top();
        if (not topNode->left and not topNode->right)
        {
            stack.pop();
            std::cout << topNode->data << " ";
        }
        else
        {
            if (topNode->right)
            {
                stack.push(topNode->right);
                topNode->right = nullptr;
            }
            if (topNode->left)
            {
                stack.push(topNode->left);
                topNode->left = nullptr;
            }
        }
    }
}

void printLevelOrder(std::shared_ptr<Node> node)
{
    if (not node) return;
    
    std::queue<std::shared_ptr<Node>> queue{};
    queue.push(node);
    
    while (not queue.empty())
    {
        auto topNode = queue.front();
        queue.pop();
        
        std::cout << topNode->data << " ";
        
        if (topNode->left) queue.push(topNode->left);
        if (topNode->right) queue.push(topNode->right);
    }
}

void printLevelOrder2(std::shared_ptr<Node> node)
{
    if (not node) return;
    
    std::queue<std::shared_ptr<Node>> queue{};
    std::list<std::shared_ptr<Node>> list{};
    queue.push(node);
    
    while (not queue.empty())
    {
        auto topNode = queue.front();
        queue.pop();
        
        list.push_front(topNode);
        
        if (topNode->left) queue.push(topNode->left);
        if (topNode->right) queue.push(topNode->right);
    }
    
    for (auto n : list)
        std::cout << n->data << " ";
}

int main() 
{ 
    auto root = std::make_shared<Node>(1);
    root->left = std::make_shared<Node>(2);
    root->right = std::make_shared<Node>(3);
    root->left->left = std::make_shared<Node>(4);
    root->left->right = std::make_shared<Node>(5);

    std::cout << "preorder recursive traversal: ";
    printPreorderRecursive(root);
    std::cout << std::endl;

    std::cout << "inorder recursive traversal: ";
    printInorderRecursive(root);
    std::cout << std::endl;

    std::cout << "postorder recursive traversal: ";
    printPostorderRecursive(root);
    std::cout << std::endl;
    
    std::cout << "preorder traversal: ";
    printPreorder(root);
    std::cout << std::endl;
    
    std::cout << "inorder traversal: ";
    printInorder(root);
    std::cout << std::endl;
    
    // std::cout << "postorder traversal: ";
    // printPostorder(root);
    // std::cout << std::endl;
    
    std::cout << "level order traversal: ";
    printLevelOrder(root);
    std::cout << std::endl;
    
    std::cout << "level order 2 traversal: ";
    printLevelOrder2(root);
    std::cout << std::endl;

    return 0; 
}
