#include <iostream>
#include <memory>

struct Node
{
    int data;
    std::shared_ptr<Node> left, right;
    Node(int data) : data(data), left(nullptr), right(nullptr) {}
}; 

void invertTree(std::shared_ptr<Node> node) 
{ 
    if (not node) return;

    std::swap(node->left, node->right);
    invertTree(node->left);
    invertTree(node->right);
}

int main() 
{ 
    auto root = std::make_shared<Node>(1);
    root->left = std::make_shared<Node>(2);
    root->right = std::make_shared<Node>(3);
    root->left->left = std::make_shared<Node>(4);
    root->left->right = std::make_shared<Node>(5);

    invertTree(root);

    return 0; 
}
