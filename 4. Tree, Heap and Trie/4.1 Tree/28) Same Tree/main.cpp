#include <iostream>
#include <memory>

struct Node
{
    int data;
    std::shared_ptr<Node> left, right;
    Node(int data) : data(data), left(nullptr), right(nullptr) {}
}; 

bool isSame(std::shared_ptr<Node> node1, std::shared_ptr<Node> node2)
{
    if (not node1 and not node2)  return true;
    if (not node1 or not node2)  return false;
    if (node1->data != node2->data) return false;
    return isSame(node1->left, node2->left) and
           isSame(node1->right, node2->right);
}

int main() 
{ 
    auto root = std::make_shared<Node>(1);
    root->left = std::make_shared<Node>(2);
    root->right = std::make_shared<Node>(3);
    root->left->left = std::make_shared<Node>(4);
    root->left->right = std::make_shared<Node>(5);

    std::cout << "is same: " << std::boolalpha << isSame(root, root) << std::endl;

    return 0;
}
