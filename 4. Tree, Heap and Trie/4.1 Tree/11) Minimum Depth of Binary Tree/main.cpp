#include <iostream>
#include <memory>
#include <queue>

struct Node
{
    int data;
    std::shared_ptr<Node> left, right;
    Node(int data) : data(data), left(nullptr), right(nullptr) {}
}; 

unsigned minDepth(std::shared_ptr<Node> node)
{
    if (not node) return 0;

    std::queue<std::shared_ptr<Node>> nodes{};
    nodes.push(node);
    std::queue<unsigned> depths{};
    depths.push(0);
    
    while (not nodes.empty())
    {
        auto topNode = nodes.front();
        nodes.pop();
        unsigned depth = depths.front();
        depths.pop();
        
        if (not topNode->left and not topNode->right) return depth;
        
        if (topNode->left)
        {
            nodes.push(topNode->left);
            depths.push(depth + 1);
        }
        if (topNode->right)
        {
            nodes.push(topNode->right);
            depths.push(depth + 1);
        }
    }
}

int main() 
{ 
    auto root = std::make_shared<Node>(1);
    root->left = std::make_shared<Node>(2);
    root->right = std::make_shared<Node>(4);
    root->left->left = std::make_shared<Node>(4);
    root->left->right = std::make_shared<Node>(3);
    root->right->right = std::make_shared<Node>(6);
    root->right->right->right = std::make_shared<Node>(7);

    std::cout << "min depth: " << minDepth(root) << std::endl;

    return 0; 
}
