#include <iostream>
#include <memory>
#include <stack>
#include <algorithm>

struct Node
{
    int data;
    std::shared_ptr<Node> left, right;
    Node(int data) : data(data), left(nullptr), right(nullptr) {}
}; 

unsigned longestConsecutiveSequence(std::shared_ptr<Node> node)
{
    unsigned max = 0;
    if (not node) return max;

    std::stack<std::shared_ptr<Node>> nodes{};
    nodes.push(node);
    std::stack<unsigned> lengths{};
    lengths.push(1);
    
    while (not nodes.empty())
    {
        auto topNode = nodes.top();
        nodes.pop();
        unsigned length = lengths.top();
        lengths.pop();
        
        max = std::max(max, length);
        
        if (topNode->right)
        {
            nodes.push(topNode->right);
            lengths.push(topNode->data + 1 == topNode->right->data ? length + 1 : 1);
        }
        if (topNode->left)
        {
            nodes.push(topNode->left);
            lengths.push(topNode->data + 1 == topNode->left->data ? length + 1 : 1);
        }
    }
    
    return max;
}

int main() 
{ 
    auto root = std::make_shared<Node>(1);
    root->left = std::make_shared<Node>(2);
    root->right = std::make_shared<Node>(4);
    root->left->left = std::make_shared<Node>(4);
    root->left->right = std::make_shared<Node>(3);

    std::cout << "longest consecutive sequence: " << longestConsecutiveSequence(root) << std::endl;

    return 0; 
}
