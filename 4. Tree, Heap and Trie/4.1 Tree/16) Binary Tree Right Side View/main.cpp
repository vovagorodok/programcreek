#include <iostream>
#include <memory>
#include <list>
#include <queue>

struct Node
{
    int data;
    std::shared_ptr<Node> left, right;
    Node(int data) : data(data), left(nullptr), right(nullptr) {}
};

std::list<int> binaryTreeRightSideView(std::shared_ptr<Node> root)
{
    if (not root) return {};
    
    std::list<int> view{};
    std::queue<std::shared_ptr<Node>> queue{};
    queue.push(root);
    
    while (not queue.empty())
    {
        unsigned size = queue.size();
        view.push_back(queue.front()->data);
        
        for (unsigned i = 0; i < size; i++)
        {
            auto node = queue.front();
            queue.pop();
        
            if (node->right) queue.push(node->right);
            if (node->left) queue.push(node->left);
        }
    }
    
    return view;
}

int main()
{
    auto root = std::make_shared<Node>(8);
    root->left = std::make_shared<Node>(3);
    root->right = std::make_shared<Node>(10);
    root->left->left = std::make_shared<Node>(1);
    root->right->right = std::make_shared<Node>(6);

    std::cout << "view: ";
    for (int val : binaryTreeRightSideView(root))
        std::cout << val << " ";

    return 0; 
}
