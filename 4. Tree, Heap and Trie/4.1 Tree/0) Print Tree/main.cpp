#include <iostream>
#include <iomanip>
#include <memory>
#include <queue>
#include <cmath>

struct Node
{
    int data;
    std::shared_ptr<Node> left, right;
    Node(int data) : data(data), left(nullptr), right(nullptr) {}
}; 

void printNode(std::shared_ptr<Node> node, unsigned lastDepth, unsigned depth, unsigned lastPosition, unsigned position, unsigned treeDepth)
{
    unsigned spacesLength = std::pow(2, treeDepth - depth);
    unsigned expectedNextPos = lastPosition + 1;
    
    if (depth > lastDepth or depth == 0)
    {
        std::cout << std::endl;
        expectedNextPos = 0;
    }

    for (unsigned pos = expectedNextPos; pos <= position; pos++)
    {
        std::cout << std::setw(pos == 0 ? spacesLength / 2 : spacesLength);
        
        if (pos == position) std::cout << node->data;
        else std::cout << ".";
    }
}

void printTree(std::shared_ptr<Node> node)
{
    if (not node) return;
    
    unsigned lastDepth = 0;
    unsigned lastPosition = 0;
    
    std::queue<std::shared_ptr<Node>> nodes{};
    nodes.push(node);
    std::queue<unsigned> depths{};
    depths.push(0);
    std::queue<unsigned> positions{};
    positions.push(0);
    
    while (not nodes.empty())
    {
        auto topNode = nodes.front();
        nodes.pop();
        unsigned depth = depths.front();
        depths.pop();
        unsigned position = positions.front();
        positions.pop();

        printNode(topNode, lastDepth, depth, lastPosition, position, 4);

        lastDepth = depth;
        lastPosition = position;
        
        if (topNode->left)
        {
            nodes.push(topNode->left);
            depths.push(depth + 1);
            positions.push(position * 2);
        }
        if (topNode->right)
        {
            nodes.push(topNode->right);
            depths.push(depth + 1);
            positions.push(position * 2 + 1);
        }
    }
}

int main() 
{ 
    auto root = std::make_shared<Node>(1);
    root->left = std::make_shared<Node>(2);
    root->right = std::make_shared<Node>(4);
    root->left->left = std::make_shared<Node>(4);
    root->left->right = std::make_shared<Node>(3);
    root->right->right = std::make_shared<Node>(6);
    root->right->right->right = std::make_shared<Node>(7);

    printTree(root);

    return 0; 
}
