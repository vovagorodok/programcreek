#include <iostream>
#include <memory>
#include <forward_list>
#include <iterator>

struct Node
{
    int data;
    std::shared_ptr<Node> left, right;
    Node(int data) : data(data), left(nullptr), right(nullptr) {}
}; 

std::shared_ptr<Node> buildTree(std::forward_list<int>::const_iterator& iterator, int begin, int end)
{
    if (begin > end) return nullptr;
    
    int mid = (begin + end) / 2;
    
    auto left = buildTree(iterator, begin, mid - 1);
    auto root = std::make_shared<Node>(*iterator);
    iterator++;
    auto right = buildTree(iterator, mid + 1, end);

    root->left = left;
    root->right = right;
    return root;
}

std::shared_ptr<Node> buildTree(const std::forward_list<int>& sortedArray)
{
    int size = std::distance(sortedArray.begin(), sortedArray.end());
    if (not size) return nullptr;
    
    auto begin = sortedArray.begin();
    return buildTree(begin, 0, size - 1);
}

int main() 
{
    std::forward_list<int> sortedArray = {0, 1, 2, 3, 4, 5, 7, 8};

    buildTree(sortedArray);

    return 0; 
}
