#include <iostream>
#include <memory>
#include <algorithm>

struct Node
{
    int data;
    std::shared_ptr<Node> left, right;
    Node(int data) : data(data), left(nullptr), right(nullptr) {}
}; 

struct MaxSumPair
{
    int maxSumOnePath;
    int maxSumTwoPaths;
};

MaxSumPair maxPathSum(std::shared_ptr<Node> node)
{ 
    if (not node)  return {};

    auto maxLeft = maxPathSum(node->left);
    auto maxRight = maxPathSum(node->right);
    
    int maxSumOnePathBefore = std::max(maxLeft.maxSumOnePath, maxRight.maxSumOnePath);
    int maxSumOnePath = std::max(node->data, node->data + maxSumOnePathBefore);
    
    int maxSumTwoPathsBefore = std::max(maxLeft.maxSumTwoPaths, maxRight.maxSumTwoPaths);
    int maxSumTwoPaths = std::max(maxSumOnePath, node->data + maxLeft.maxSumOnePath + maxRight.maxSumOnePath);
    
    return {maxSumOnePath, std::max(maxSumTwoPathsBefore, maxSumTwoPaths)};
}

int main() 
{ 
    auto root = std::make_shared<Node>(1);
    root->left = std::make_shared<Node>(2);
    root->right = std::make_shared<Node>(3);
    root->left->left = std::make_shared<Node>(4);
    root->left->right = std::make_shared<Node>(5);

    std::cout << "max path sum: " << maxPathSum(root).maxSumTwoPaths << std::endl;

    return 0; 
}
