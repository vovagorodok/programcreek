#include <iostream>
#include <memory>
#include <stack>

struct Node
{
    int data;
    std::shared_ptr<Node> left, right;
    Node(int data) : data(data), left(nullptr), right(nullptr) {}
}; 

void printKthSmallestElementInBST(std::shared_ptr<Node> root, unsigned k) 
{
    if (not node) return;
    
    std::stack<std::shared_ptr<Node>> stack{};
    auto topNode = node;
    while (topNode)
    {
        stack.push(topNode);
        topNode = topNode->left;
    }
    
    while (not stack.empty())
    {
        topNode = stack.top();
        stack.pop();
        
        if (not k--) std::cout << topNode->data << " ";
        
        topNode = topNode->right;
        while (topNode)
        {
            stack.push(topNode);
            topNode = topNode->left;
        }
    }
}

int main() 
{ 
    auto root = std::make_shared<Node>(6);
    root->left = std::make_shared<Node>(4);
    root->right = std::make_shared<Node>(7);
    root->left->left = std::make_shared<Node>(3);
    root->left->right = std::make_shared<Node>(5);

    std::cout << "k-th smallest element in BST: ";
    printKthSmallestElementInBST(root, 2);

    return 0; 
}
