#include <iostream>
#include <memory>

struct Node
{
    int data;
    std::shared_ptr<Node> left, right;
    Node(int data) : data(data), left(nullptr), right(nullptr) {}
}; 

bool isSymmetric(std::shared_ptr<Node> left, std::shared_ptr<Node> right)
{
    if (not left and not right) return true;
    if (not left or not right) return false;
    if (left->data != right->data) return false;
    
    return isSymmetric(left->left, right->right) and
           isSymmetric(left->right, right->left);
}

bool isSymmetric(std::shared_ptr<Node> root)
{
    if (not root) return false;
    
    return isSymmetric(root->left, root->right);
}

int main() 
{ 
    auto root = std::make_shared<Node>(1);
    root->left = std::make_shared<Node>(2);
    root->right = std::make_shared<Node>(2);
    root->left->left = std::make_shared<Node>(5);
    root->right->right = std::make_shared<Node>(5);

    std::cout << "is symmetric: " << std::boolalpha << isSymmetric(root) << std::endl;

    return 0; 
}
