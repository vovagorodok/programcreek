#include <iostream>

unsigned numOfUnigueBST(unsigned n)
{
    unsigned nums[n + 1];
    
    nums[0] = 1;
    nums[1] = 1;
    
    for (unsigned num = 2; num <= n; num++)
    {
        nums[num] = 0;
        for (unsigned i = 0; i < num; i++)
            nums[num] += nums[i] * nums[num - 1 - i];
    }
    
    return nums[n];
}

int main()
{
    std::cout << "num of unigue BST: " << numOfUnigueBST(4) << std::endl;
    return 0;
}
