#include <iostream>
#include <memory>
#include <stack>
#include <algorithm>

struct Node
{
    int data;
    std::shared_ptr<Node> left, right;
    Node(int data) : data(data), left(nullptr), right(nullptr) {}
}; 

bool isPathSum(std::shared_ptr<Node> node, int expectedSum)
{
    if (not node) return false;

    std::stack<std::shared_ptr<Node>> nodes{};
    nodes.push(node);
    std::stack<int> sums{};
    sums.push(node->data);
    
    while (not nodes.empty())
    {
        auto topNode = nodes.top();
        nodes.pop();
        int sum = sums.top();
        sums.pop();
        
        if (sum == expectedSum) return true;
        
        if (topNode->right)
        {
            nodes.push(topNode->right);
            sums.push(sum + topNode->right->data);
        }
        if (topNode->left)
        {
            nodes.push(topNode->left);
            sums.push(sum + topNode->left->data);
        }
    }
    
    return false;
}

int main() 
{ 
    auto root = std::make_shared<Node>(1);
    root->left = std::make_shared<Node>(2);
    root->right = std::make_shared<Node>(4);
    root->left->left = std::make_shared<Node>(4);
    root->left->right = std::make_shared<Node>(3);

    std::cout << "is path sum: " << std::boolalpha << isPathSum(root, 6) << std::endl;

    return 0; 
}
