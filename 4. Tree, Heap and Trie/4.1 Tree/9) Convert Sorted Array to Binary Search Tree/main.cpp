#include <iostream>
#include <memory>
#include <vector>

struct Node
{
    int data;
    std::shared_ptr<Node> left, right;
    Node(int data) : data(data), left(nullptr), right(nullptr) {}
}; 

std::shared_ptr<Node> buildTree(const std::vector<int>& sortedArray, int begin, int end)
{
    if (begin > end) return nullptr;
    
    int mid = (begin + end) / 2;
    auto root = std::make_shared<Node>(sortedArray[mid]);

    root->left = buildTree(sortedArray, begin, mid - 1);
    root->right = buildTree(sortedArray, mid + 1, end);
    return root;
}

std::shared_ptr<Node> buildTree(const std::vector<int>& sortedArray)
{
    if (not sortedArray.size()) return nullptr;
    
    return buildTree(sortedArray, 0, sortedArray.size() - 1);
}

int main() 
{
    std::vector<int> sortedArray = {0, 1, 2, 3, 4, 5, 7, 8};

    buildTree(sortedArray);

    return 0; 
}
