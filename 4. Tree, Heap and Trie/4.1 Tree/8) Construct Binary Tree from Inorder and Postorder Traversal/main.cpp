#include <iostream>
#include <memory>
#include <vector>

struct Node
{
    int data;
    std::shared_ptr<Node> left, right;
    Node(int data) : data(data), left(nullptr), right(nullptr) {}
}; 

std::shared_ptr<Node> buildTree(const std::vector<int>& inorder, const std::vector<int>& postorder,
                                int inBegin, int inEnd, int postBegin, int postEnd)
{
    if (inBegin > inEnd or postBegin > postEnd) return nullptr;
    
    int rootValue = postorder[postEnd];
    auto root = std::make_shared<Node>(rootValue);

    unsigned rootInorderPos = inBegin;
    while (rootInorderPos < inEnd and inorder[rootInorderPos] != rootValue)
        rootInorderPos++;

    int size = rootInorderPos - inBegin;

    root->left = buildTree(inorder, postorder, inBegin, inBegin + size - 1, postBegin, postBegin + size - 1);
    root->right = buildTree(inorder, postorder, inBegin + size + 1, inEnd, postBegin + size, postEnd - 1);
    return root;
}

std::shared_ptr<Node> buildTree(const std::vector<int>& inorder, const std::vector<int>& postorder)
{
    if (not inorder.size() or inorder.size() != postorder.size()) return nullptr;
    
    return buildTree(inorder, postorder, 0, inorder.size() - 1, 0, postorder.size() - 1);
}

int main() 
{
    std::vector<int> inorder = {4, 2, 5, 1, 6, 7, 3, 8};
    std::vector<int> postorder = {4, 5, 2, 6, 7, 8, 3, 1};

    buildTree(inorder, postorder);

    return 0; 
}
