#include <iostream>
#include <memory>
#include <stack>

struct Node
{
    int data;
    std::shared_ptr<Node> left, right;
    Node(int data) : data(data), left(nullptr), right(nullptr) {}
}; 

struct BSTIterator
{
    BSTIterator(std::shared_ptr<Node> root)
    {
        pushAllLeftToStack(root);
        findSmallest();
    }
    
    void pushAllLeftToStack(std::shared_ptr<Node> node)
    {
        while (node)
        {
            stack.push(node);
            node = node->left;
        }
    }
    
    void findSmallest()
    {
        if (stack.empty())
        {
            smallest = nullptr;
            return;
        }
        
        smallest = stack.top();
        stack.pop();
        pushAllLeftToStack(smallest->right);
    }
    
    bool hasNext()
    {
        return smallest != nullptr;
    }
    
    int next()
    {
        int ret = smallest->data;
        findSmallest();
        return ret;
    }
    
    std::stack<std::shared_ptr<Node>> stack;
    std::shared_ptr<Node> smallest;
};

int main() 
{ 
    auto root = std::make_shared<Node>(8);
    root->left = std::make_shared<Node>(3);
    root->right = std::make_shared<Node>(10);
    root->left->left = std::make_shared<Node>(1);
    root->left->right = std::make_shared<Node>(6);
    
    BSTIterator it(root);

    std::cout << "values: ";
    while (it.hasNext()) std::cout << it.next() << " ";

    return 0; 
}
