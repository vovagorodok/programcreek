#include <iostream>
#include <memory>
#include <limits>

struct Node
{
    int data;
    std::shared_ptr<Node> left, right;
    Node(int data) : data(data), left(nullptr), right(nullptr) {}
}; 

bool isBSTValid(std::shared_ptr<Node> node,
                int min = std::numeric_limits<int>::min(),
                int max = std::numeric_limits<int>::max())
{
    if (not node)
        return true;
    if (node->data <= min or node->data >= max)
        return false;
    return isBSTValid(node->left, min, node->data) and
           isBSTValid(node->right, node->data, max);
}

int main() 
{ 
    auto root = std::make_shared<Node>(6);
    root->left = std::make_shared<Node>(4);
    root->right = std::make_shared<Node>(7);
    root->left->left = std::make_shared<Node>(3);
    root->left->right = std::make_shared<Node>(5);

    std::cout << "is BST valid: " << std::boolalpha << isBSTValid(root) << std::endl;

    return 0; 
}
