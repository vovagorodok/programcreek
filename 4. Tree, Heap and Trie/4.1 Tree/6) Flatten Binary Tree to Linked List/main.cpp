#include <iostream>
#include <memory>
#include <stack>

struct Node
{
    int data;
    std::shared_ptr<Node> left, right;
    Node(int data) : data(data), left(nullptr), right(nullptr) {}
}; 

void printLikeLinkedList(std::shared_ptr<Node> node)
{
    auto nextNode = node;
    while (nextNode)
    {
        std::cout << nextNode->data << " ";
        nextNode = nextNode->right;
    }
}

void changeFlattenBinaryTreeToLinkedList(std::shared_ptr<Node> node)
{
    if (not node) return;
    
    std::stack<std::shared_ptr<Node>> stack{};
    stack.push(node);
    
    while (not stack.empty())
    {
        auto topNode = stack.top();
        stack.pop();
        
        if (topNode->right) stack.push(topNode->right);
        if (topNode->left) stack.push(topNode->left);
        
        topNode->left = nullptr;
        if (not stack.empty()) topNode->right = stack.top();
        else topNode->right = nullptr;
    }
}

int main() 
{ 
    auto root = std::make_shared<Node>(1);
    root->left = std::make_shared<Node>(2);
    root->right = std::make_shared<Node>(3);
    root->left->left = std::make_shared<Node>(4);
    root->left->right = std::make_shared<Node>(5);

    changeFlattenBinaryTreeToLinkedList(root);
    printLikeLinkedList(root);

    return 0; 
}
