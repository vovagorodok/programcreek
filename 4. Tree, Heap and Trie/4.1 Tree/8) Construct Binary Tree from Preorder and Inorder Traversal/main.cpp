#include <iostream>
#include <memory>
#include <vector>

struct Node
{
    int data;
    std::shared_ptr<Node> left, right;
    Node(int data) : data(data), left(nullptr), right(nullptr) {}
}; 

std::shared_ptr<Node> buildTree(const std::vector<int>& inorder, const std::vector<int>& preorder,
                                int inBegin, int inEnd, int preBegin, int preEnd)
{
    if (inBegin > inEnd or preBegin > preEnd) return nullptr;
    
    int rootValue = preorder[preBegin];
    auto root = std::make_shared<Node>(rootValue);

    unsigned rootInorderPos = inBegin;
    while (rootInorderPos < inEnd and inorder[rootInorderPos] != rootValue)
        rootInorderPos++;

    int size = rootInorderPos - inBegin;

    root->left = buildTree(inorder, preorder, inBegin, inBegin + size - 1, preBegin + 1, preBegin + size);
    root->right = buildTree(inorder, preorder, inBegin + size + 1, inEnd, preBegin + size + 1, preEnd);
    return root;
}

std::shared_ptr<Node> buildTree(const std::vector<int>& inorder, const std::vector<int>& preorder)
{
    if (not inorder.size() or inorder.size() != preorder.size()) return nullptr;
    
    return buildTree(inorder, preorder, 0, inorder.size() - 1, 0, preorder.size() - 1);
}

int main() 
{
    std::vector<int> inorder = {4, 2, 5, 1, 6, 7, 3, 8};
    std::vector<int> preorder = {1, 2, 4, 5, 3, 7, 6, 8};

    buildTree(inorder, preorder);

    return 0; 
}
