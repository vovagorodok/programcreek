#include <iostream>
#include <memory>

struct Node
{
    int data;
    std::shared_ptr<Node> left, right;
    Node(int data) : data(data), left(nullptr), right(nullptr) {}
}; 

int sumRootToLeafNumbers(std::shared_ptr<Node> node, int concat = 0)
{
    if (not node)  return 0;
    
    concat = (concat * 10) + node->data;
    if (not node->left and not node->right) return concat;

    return sumRootToLeafNumbers(node->left, concat) +
           sumRootToLeafNumbers(node->right, concat);
}

int main() 
{ 
    auto root = std::make_shared<Node>(1);
    root->left = std::make_shared<Node>(2);
    root->right = std::make_shared<Node>(3);
    root->left->left = std::make_shared<Node>(4);
    root->left->right = std::make_shared<Node>(5);

    std::cout << "sum root to leaf numbers: " << sumRootToLeafNumbers(root)  << std::endl;

    return 0; 
}
