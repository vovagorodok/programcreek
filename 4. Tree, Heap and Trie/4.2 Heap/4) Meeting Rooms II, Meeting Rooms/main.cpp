#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <functional>
#include <queue>

struct Interval
{
    unsigned begin;
    unsigned end;
};

unsigned numberOfRooms(const std::list<Interval>& intervals)
{
    std::priority_queue<unsigned, std::vector<unsigned>, std::greater<unsigned>> begins{};
    std::priority_queue<unsigned, std::vector<unsigned>, std::greater<unsigned>> ends{};
    
    for (Interval interval : intervals)
    {
        begins.push(interval.begin);
        ends.push(interval.end);
    }
    
    unsigned maxRoomsNumber = 0;
    unsigned counter = 0;

    while (not begins.empty() and not ends.empty())
    {
        unsigned begin = begins.top();
        unsigned end = ends.top();
        
        if (begin <= end)
        {
            counter++;
            begins.pop();
        }
        else
        {
            counter--;
            ends.pop();
        }
        
        maxRoomsNumber = std::max(maxRoomsNumber, counter);
    }
   
    return maxRoomsNumber;
}

int main()
{ 
    std::list<Interval> intervals =
        {{2, 15}, {36, 45}, {9, 29}, {16, 23}, {4, 9}};
    
    std::cout << "number of reguired rooms: " << numberOfRooms(intervals) << std::endl;
    
    return 0;
}
