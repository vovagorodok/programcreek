#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <functional>

void push(int num, std::vector<int>& heap, auto cmp)
{
    heap.push_back(num);
    std::push_heap(heap.begin(), heap.end(), cmp);
}

int pop(std::vector<int>& heap, auto cmp)
{
    int ret = heap.front();
    std::pop_heap(heap.begin(), heap.end(), cmp);
    heap.pop_back();
    return ret;
}

void add(int num, std::vector<int>& heapMin, std::vector<int>& heapMax)
{
    push(num, heapMin, std::greater<int>());
    push(pop(heapMin, std::greater<int>()), heapMax, std::less<int>());
    
    if (heapMin.size() < heapMax.size())
        push(pop(heapMax, std::less<int>()), heapMin, std::greater<int>());
}

double median(std::vector<int>& heapMin, std::vector<int>& heapMax)
{
    if (heapMin.size() > heapMax.size()) return heapMin.front();
    
    return (heapMin.front() + heapMax.front()) / 2.0;
}

void printStreamOfMedians(const std::list<int>& stream)
{
    std::vector<int> heapMin{}, heapMax{};
    
    for (int num : stream)
    {
        add(num, heapMin, heapMax);
        std::cout << median(heapMin, heapMax) << " ";
    }
}

int main() 
{ 
    std::list<int> stream = {1, 3, 2, 4, 6, 0, 9, 10, 11};
    
    std::cout << "medians: ";
    printStreamOfMedians(stream);

    return 0;
}
