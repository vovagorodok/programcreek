#include <iostream>
#include <vector>
#include <list>
#include <algorithm>

using SortedListsOfNumbers = std::list<std::list<int>>;

struct HeapNode
{
    std::list<int>::const_iterator it;
    std::list<int>::const_iterator end;
};

std::list<int> mergeSortedLists(const SortedListsOfNumbers& lists)
{
    std::list<int> merged{};
    std::vector<HeapNode> heap{};

    auto cmp = [](const HeapNode& lhs, const HeapNode& rhs)
        { return *lhs.it > *rhs.it; };
        
    for (const auto& list : lists)
        heap.push_back(HeapNode{list.begin(), list.end()});
    std::make_heap(heap.begin(), heap.end(), cmp);
    
    while (not heap.empty())
    {
        HeapNode min = heap.front();
        std::pop_heap(heap.begin(), heap.end(), cmp);
        
        merged.push_back(*min.it);
        min.it++;
        
        if (min.it != min.end)
        {
            heap.back() = min;
            std::push_heap(heap.begin(), heap.end(), cmp);
        }
        else heap.pop_back();
    }
    
    return merged;
}

int main() 
{ 
    SortedListsOfNumbers lists =
        {{1, 3},
         {2, 4, 6},
         {0, 9, 10, 11}};
    
    std::cout << "merged list: ";
    for (int el : mergeSortedLists(lists))
        std::cout << el << " ";

    return 0;
}
