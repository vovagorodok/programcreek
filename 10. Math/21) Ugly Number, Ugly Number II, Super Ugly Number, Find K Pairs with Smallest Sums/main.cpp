#include <iostream>

bool isUgly(unsigned n)
{
    if (n == 0) return false;
    if (n == 1) return true;
    
    if (n % 2 == 0) return isUgly(n / 2);
    if (n % 3 == 0) return isUgly(n / 3);
    if (n % 5 == 0) return isUgly(n / 5);
    
    return false;
}

int main()
{
    std::cout << "is ugly: " << std::boolalpha << isUgly(6) << std::endl;
    std::cout << "is ugly: " << std::boolalpha << isUgly(8) << std::endl;
    std::cout << "is ugly: " << std::boolalpha << isUgly(14) << std::endl;
    
    return 0;
}
