#include <iostream>

int reverseInt(int x)
{
    int rev = 0;
    while (x != 0)
    {
        rev = rev * 10 + x % 10;
        x /= 10;
    }
    return rev;
}

int main()
{
    std::cout << "reverse int: " << reverseInt(-123) << std::endl;
    
    return 0;
}
