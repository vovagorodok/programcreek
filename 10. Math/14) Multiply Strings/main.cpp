#include <iostream>
#include <algorithm>
#include <string>

std::string multiplyStrings(std::string str1, std::string str2)
{
    std::reverse(str1.begin(), str1.end());
    std::reverse(str2.begin(), str2.end());
    const unsigned mulsSize = str1.size() + str2.size();
    unsigned muls[mulsSize]{};

    for (unsigned i = 0; i < str1.size(); i++)
        for (unsigned j = 0; j < str2.size(); j++)
            muls[i + j] += (str1[i] - '0') * (str2[j] - '0');
            
    for (unsigned i = 0; i < mulsSize - 1; i++)
    {
        muls[i + 1] += muls[i] / 10;
        muls[i] %= 10;
    }
    
    unsigned lastNotZero = mulsSize - 1;
    while (muls[lastNotZero] == 0) lastNotZero--;

    std::string res{};
    for (int i = lastNotZero; i >= 0; i--)
        res.push_back(muls[i] + '0');
    
    return res;
}

int main()
{
    std::cout << "strings multiplication: "
              << multiplyStrings("4154", "51454") << std::endl;
              
    std::cout << "strings multiplication: "
              << multiplyStrings("654154154151454545415415454", "63516561563156316545145146514654") << std::endl;
    
    return 0;
}
