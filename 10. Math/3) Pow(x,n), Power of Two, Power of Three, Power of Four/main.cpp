#include <iostream>
#include <cmath>

double pow(double x, int n)
{
    if (n == 0) return 1;
    if (n < 0) return pow(x, -n);
    
    double subPow = pow(x, n / 2);
    return n % 2 ?
           subPow * subPow * x:
           subPow * subPow;
}

bool isPowOf2(int x)
{
    return x > 0 and (x & x - 1) == 0;
}

double logb(unsigned base, double x)
{
    return std::log(x) / std::log(base);
}

bool isPowOfN(unsigned n, double x)
{
    return x == std::pow(n, std::floor(logb(n, x)));
}

int main()
{
    std::cout << "pow: " << pow(3, 3) << std::endl;
    
    std::cout << "is power of 2: " << std::boolalpha << isPowOf2(5) << std::endl;
    std::cout << "is power of 2: " << std::boolalpha << isPowOf2(8) << std::endl;
    
    std::cout << "is power of 3: " << std::boolalpha << isPowOfN(3, 15) << std::endl;
    std::cout << "is power of 3: " << std::boolalpha << isPowOfN(3, 27) << std::endl;
    
    std::cout << "is power of 4: " << std::boolalpha << isPowOfN(4, 15) << std::endl;
    std::cout << "is power of 4: " << std::boolalpha << isPowOfN(4, 16) << std::endl;
    
    return 0;
}
