#include <iostream>
#include <set>
#include <string>

std::string printRecurringSequenceInAFraction(unsigned numerator, unsigned denominator)
{
    std::string res{};
    std::set<unsigned> set{};
    unsigned reminder = numerator % denominator;
    unsigned value;
    
    while (reminder != 0 and not set.count(reminder))
    {
        set.insert(reminder);
        reminder *= 10;
        value = reminder / denominator;
        res += std::to_string(value);
        reminder %= denominator;
    }

    return reminder ? res : "NA";
}

int main()
{
    std::cout << "recurring sequence in a fraction: " << printRecurringSequenceInAFraction(1, 2) << std::endl;
    std::cout << "recurring sequence in a fraction: " << printRecurringSequenceInAFraction(1, 3) << std::endl;
    std::cout << "recurring sequence in a fraction: " << printRecurringSequenceInAFraction(50, 22) << std::endl;
    
    return 0;
}
