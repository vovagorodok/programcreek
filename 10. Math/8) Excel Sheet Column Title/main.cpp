#include <iostream>
#include <string>

constexpr unsigned Base = 'Z' - 'A' + 1;

std::string numberToexcelSheetColumn(unsigned num)
{
    std::string res{};
    unsigned reminder;
    
    while (num != 0)
    {
        num--;
        reminder = num % Base;
        res.insert(0, 1, 'A' + reminder);
        num /= Base;
    }

    return res;
}

int main()
{
    std::cout << "column name: " << numberToexcelSheetColumn(1) << std::endl;
    std::cout << "column name: " << numberToexcelSheetColumn(26) << std::endl;
    std::cout << "column name: " << numberToexcelSheetColumn(27) << std::endl;
    std::cout << "column name: " << numberToexcelSheetColumn(703) << std::endl;
    
    return 0;
}
