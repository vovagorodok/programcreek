#include <iostream>
#include <algorithm>
#include <cmath>

int integerBreakDP(int n)
{
    int dp[n + 1]{};
    
    for (int i = 1; i < n; i++)
        for (int j = 1; j < i + 1; j++)
            if (i + j <= n)
                dp[i + j] = std::max(dp[i + j], std::max(dp[i], i) * std::max(dp[j], j));
    
    return dp[n];
}

int integerBreak(int n)
{
    if (n == 0) return 0;
    if (n == 1 or n == 2) return 1;
    if (n == 3) return 2;
    if (n == 4) return 4;
    
    int numberOf3 = n / 3;
    int reminder = 0;
    if (n % 3 == 0) reminder = 1;
    if (n % 3 == 2) reminder = 2;
    if (n % 3 == 1) { reminder = 4; numberOf3--; }

    return std::pow(3, numberOf3) * reminder;
}

int main()
{
    std::cout << "integer break dp: " << integerBreakDP(4) << std::endl;
    std::cout << "integer break dp: " << integerBreakDP(10) << std::endl;
    
    std::cout << "integer break: " << integerBreak(4) << std::endl;
    std::cout << "integer break: " << integerBreak(10) << std::endl;
    
    return 0;
}
