#include <iostream>

unsigned addDigits(unsigned n)
{
    unsigned sum = 0;
    while (n != 0)
    {
        sum += n % 10;
        n /= 10;
    }
    
    return sum > 9 ? addDigits(sum) : sum;
}

unsigned addDigitsMath(unsigned n)
{
    return n - 9 * ((n - 1) / 9);
}

int main()
{
    std::cout << "add digits: " << addDigits(38) << std::endl;
    std::cout << "add digits math: " << addDigitsMath(38) << std::endl;
    
    return 0;
}
