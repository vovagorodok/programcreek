#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <algorithm>

struct Point
{
    double x;
    double y;
};

struct Line
{
    double a;
    double b;
};

Line calcLine(const Point& p1, const Point& p2)
{
    Line line;
    line.a = (p2.y - p1.y) / (p2.x - p1.x);
    line.b = p1.y - line.a * p1.x;
    return line;
}

unsigned maxPointsOnALine(std::vector<Point> points)
{
    auto cmpLine = [](const Line& lhs, const Line& rhs)
        { return lhs.a < rhs.a or (lhs.a == rhs.a and lhs.b < rhs.b); };
    std::map<Line, unsigned, decltype(cmpLine)> map(cmpLine);
    unsigned max = 0;
    
    for (unsigned i = 0; i < points.size(); i++)
    {
        map.clear();
        for (unsigned j = i + 1; j < points.size(); j++)
        {
            Line line = calcLine(points[i], points[j]);
            map[line]++;
            max = std::max(max, map[line]);
        }
    }
    
    if (max > 0) max++;
    if (points.size() == 1) max = 1;
    
    return max;
}

int main()
{
    std::cout << "max points on a line: " << maxPointsOnALine(
        {{-1, 1}, {0, 0}, {1, 1}, {2, 2}, {3, 3}, {3, 4}}) << std::endl;

    return 0;
}
