#include <iostream>
#include <vector>
#include <algorithm>

unsigned countPrimes(unsigned n)
{
    if (n < 2) return 0;
    
    std::vector<bool> eratosthenesSieve(n, true);
    eratosthenesSieve[0] = eratosthenesSieve[1] = false;
    
    for (unsigned i = 2; i < n; i++)
        if (eratosthenesSieve[i])
            for (unsigned j = i + i; j < n; j += i)
                eratosthenesSieve[j] = false;
                
    return std::count(eratosthenesSieve.begin(), eratosthenesSieve.end(), true);
}

int main()
{
    std::cout << "primes number: " << countPrimes(10) << std::endl;
    std::cout << "primes number: " << countPrimes(20) << std::endl;
    
    return 0;
}
