#include <iostream>
#include <string_view>

constexpr unsigned Base = 'Z' - 'A' + 1;

unsigned excelSheetColumnToNumber(std::string_view str)
{
    unsigned res = 0;
    
    for (auto ch : str)
    {
        res *= Base;
        res += ch - 'A' + 1; 
    }
    
    return res;
}

int main()
{
    std::cout << "column number: " << excelSheetColumnToNumber("A") << std::endl;
    std::cout << "column number: " << excelSheetColumnToNumber("Z") << std::endl;
    std::cout << "column number: " << excelSheetColumnToNumber("AA") << std::endl;
    std::cout << "column number: " << excelSheetColumnToNumber("AAA") << std::endl;
    
    return 0;
}
