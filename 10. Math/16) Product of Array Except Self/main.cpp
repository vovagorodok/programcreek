#include <iostream>
#include <list>

std::list<int> productOfArrayExceptSelf(std::list<int> array)
{
    std::list<int> prods(array.begin(), array.end());
    int prodOfArray = 1;
    
    for (int num : array) prodOfArray *= num;
    for (int& prod : prods) prod = prodOfArray / prod;
    
    return prods;
}

int main()
{
    std::cout << "product of array except self:" << std::endl;
    for (int prod : productOfArrayExceptSelf({1, 2, 3, 4}))
        std::cout << prod << " ";

    return 0;
}
