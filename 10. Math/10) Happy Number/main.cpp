#include <iostream>

unsigned squareSum(unsigned num)
{
    unsigned sum = 0;
    unsigned reminder;
    
    while (num != 0)
    {
        reminder = num % 10;
        sum += reminder * reminder;
        num /= 10;
    }
    
    return sum;
}

bool isHappyNumber(unsigned num)
{
    unsigned slow = num;
    unsigned fast = num;
    
    do
    {
        slow = squareSum(slow);
        fast = squareSum(squareSum(fast));
    } while (slow != fast);
    
    return slow == 1;
}

int main()
{
    std::cout << "is happy number: " << std::boolalpha << isHappyNumber(19) << std::endl;
    std::cout << "is happy number: " << std::boolalpha << isHappyNumber(20) << std::endl;
    
    return 0;
}
