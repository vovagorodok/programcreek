#include <iostream>
#include <cmath>

int devide(int a, int b)
{
    int sign = a > 0 xor b > 0 ? -1 : 1;
    a = std::abs(a);
    b = std::abs(b);
    
    int res = 0;
    while (a > b)
    {
        a -= b;
        res++;
    }
    
    return sign * res;
}

int main()
{
    std::cout << "devision: " << devide(1, 2) << std::endl;
    std::cout << "devision: " << devide(10, 3) << std::endl;
    std::cout << "devision: " << devide(43, -8) << std::endl;
    
    return 0;
}
