#include <iostream>

int reverseInt(int x)
{
    int rev = 0;
    while (x != 0)
    {
        rev = rev * 10 + x % 10;
        x /= 10;
    }
    return rev;
}

bool isPalidrome(int x)
{
    int rev = reverseInt(x);
    
    while (x != 0)
    {
        if (x % 10 != rev % 10) return false;
        x /= 10;
        rev /= 10;
    }
    
    return true;
}

int main()
{
    std::cout << "is palidrome: " << std::boolalpha << isPalidrome(12321) << std::endl;
    
    return 0;
}
