#include <iostream>

unsigned factorialTrailingZeroes(unsigned n)
{
    unsigned count = 0;
    
    for (unsigned i = 5; i <= n; i *= 5)
        count += n / i;
    
    return count;
}

int main()
{
    std::cout << "factorial trailing zeroes: " << factorialTrailingZeroes(5) << std::endl;
    std::cout << "factorial trailing zeroes: " << factorialTrailingZeroes(11) << std::endl;
    std::cout << "factorial trailing zeroes: " << factorialTrailingZeroes(20) << std::endl;
    std::cout << "factorial trailing zeroes: " << factorialTrailingZeroes(100) << std::endl;
    
    return 0;
}
