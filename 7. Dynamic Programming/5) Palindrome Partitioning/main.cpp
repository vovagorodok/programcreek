#include <iostream>
#include <string_view>
#include <list>

using Palidroms = std::list<std::string_view>;
using AllPalidroms = std::list<Palidroms>;

Palidroms centralPalidroms(std::string_view str,
                           int rangeBegin, int rangeEnd,
                           int palidromBegin, int palidromEnd)
{
    Palidroms ret{};
    
    while (palidromBegin >= rangeBegin and
           palidromEnd <= rangeEnd and
           str[palidromBegin] == str[palidromEnd])
    {
        int palidromSize = palidromEnd - palidromBegin + 1;
        if (palidromSize > 1)
            ret.push_back(str.substr(palidromBegin, palidromSize));
        
        palidromBegin--; palidromEnd++;
    }

    return ret;
}

Palidroms allCharsAsPalidroms(std::string_view str, int rangeBegin, int rangeEnd)
{
    Palidroms ret{};
    for (int i = rangeBegin; i <= rangeEnd; i++)
        ret.push_back(str.substr(i, 1));
    return ret;
}

AllPalidroms palidromesPermutations(std::string_view centralPalidrom,
                                    Palidroms leftPalidroms,
                                    AllPalidroms rightPalidroms)
{
    AllPalidroms ret{};
    
    for (Palidroms& right : rightPalidroms)
    {
        Palidroms left = leftPalidroms;
        Palidroms center{centralPalidrom};
        center.splice(center.begin(), std::move(left));
        center.splice(center.end(), std::move(right));
        ret.push_back(std::move(center));
    }
    
    return ret;
}

AllPalidroms palindromePartitioning(std::string_view str, int rangeBegin, int rangeEnd)
{
    if (rangeBegin < 0 or rangeEnd >= str.size() or rangeBegin > rangeEnd)
        return {};
    
    AllPalidroms ret{allCharsAsPalidroms(str, rangeBegin, rangeEnd)};

    for (int i = rangeBegin; i <= rangeEnd; i++)
    {
        for (std::string_view palidrom : centralPalidroms(str, rangeBegin, rangeEnd, i, i))
        {
            int offset = palidrom.size() / 2 + 1;
            Palidroms leftPalidroms = allCharsAsPalidroms(str, rangeBegin, i - offset);
            AllPalidroms rightPalidroms = palindromePartitioning(str, i + offset, rangeEnd);
            ret.splice(ret.end(),
                       palidromesPermutations(palidrom, std::move(leftPalidroms), std::move(rightPalidroms)));
        }
        for (std::string_view palidrom : centralPalidroms(str, rangeBegin, rangeEnd, i, i + 1))
        {
            int offset = palidrom.size() / 2;
            Palidroms leftPalidroms = allCharsAsPalidroms(str, rangeBegin, i - offset);
            AllPalidroms rightPalidroms = palindromePartitioning(str, i + 1 + offset, rangeEnd);
            ret.splice(ret.end(),
                       palidromesPermutations(palidrom, std::move(leftPalidroms), std::move(rightPalidroms)));
        }
    }
    
    return ret;
}

AllPalidroms palindromePartitioning(std::string_view str)
{
    return palindromePartitioning(str, 0, str.size() - 1);
}

int main()
{
    std::cout << "all possible palindrome partitioning:" << std::endl;
    for (Palidroms& palidroms : palindromePartitioning("addvfgasdffdsav"))
    {
        for (std::string_view palidrom : palidroms)
            std::cout << palidrom << " ";
        std::cout << std::endl;
    }
    
    return 0;
}
