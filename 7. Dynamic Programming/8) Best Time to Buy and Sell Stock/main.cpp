#include <iostream>
#include <list>
#include <algorithm>

int maxProfitForOneTransaction(std::list<int> prices)
{
    int minPrice = prices.front();
    int maxProfit = 0;
    
    for (int price : prices)
    {
        minPrice = std::min(minPrice, price);
        maxProfit = std::max(maxProfit, price - minPrice);
    }
    
    return maxProfit;
}

int main()
{
    std::cout << "max profit for one transaction: "
              << maxProfitForOneTransaction({100, 180, 260, 310, 40, 535, 695}) << std::endl;
    
    return 0;
}
