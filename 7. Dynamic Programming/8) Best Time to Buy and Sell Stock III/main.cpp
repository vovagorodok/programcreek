#include <iostream>
#include <vector>
#include <algorithm>

int maxProfitForTwoTransactions(std::vector<int> prices)
{
    std::vector<int> left(prices.size());
    std::vector<int> right(prices.size());
    
    left.front() = 0;
    int min = prices.front();
    for (int i = 1; i < prices.size(); i++)
    {
        min = std::min(min, prices[i]);
        left[i] = std::max(left[i - 1], prices[i] - min);
    }
    
    right.back() = 0;
    int max = prices.back();
    for (int i = prices.size() - 2; i >= 0; i--)
    {
        max = std::max(max, prices[i]);
        right[i] = std::max(right[i + 1], max - prices[i]);
    }
    
    int maxProfit = 0;
    for (int i = 0; i < prices.size(); i++)
        maxProfit = std::max(maxProfit, left[i] + right[i]);
    
    return maxProfit;
}

int main()
{
    std::cout << "max profit for two transactions: "
              << maxProfitForTwoTransactions({1, 4, 5, 7, 6, 3, 2, 9}) << std::endl;
    
    return 0;
}
