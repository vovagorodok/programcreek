#include <iostream>
#include <vector>
#include <algorithm>

unsigned minJumps(std::vector<unsigned> jumps)
{
    unsigned max = 0;
    unsigned lastMax = 0;
    unsigned steps = 0;
    
    for (unsigned i = 0; i < jumps.size(); i++)
    {
        if (i > lastMax)
        {
            lastMax = max;
            steps++;
        }
        
        if (i > max) return 0;
        else max = std::max(max, i + jumps[i]);
    }
    
    return steps;
}

int main()
{
    std::cout << "min jumps: " << minJumps({2, 3, 1, 1, 4}) << std::endl;
    
    return 0;
}
