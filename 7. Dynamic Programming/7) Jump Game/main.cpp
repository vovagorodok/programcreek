#include <iostream>
#include <algorithm>
#include <vector>

bool canJump(std::vector<unsigned> jumps)
{
    unsigned max = 0;
    for (unsigned i = 0; i < jumps.size(); i++)
        if (i <= max) max = std::max(max, i + jumps[i]);
        else return false;
    return true;
}

int main()
{
    std::cout << "can jump: " << std::boolalpha << canJump({2, 3, 1, 1, 4}) << std::endl;
    std::cout << "can jump: " << std::boolalpha << canJump({3, 2, 1, 0, 4}) << std::endl;
    
    return 0;
}
