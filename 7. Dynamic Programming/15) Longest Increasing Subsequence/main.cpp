#include <iostream>
#include <list>
#include <map>
#include <vector>
#include <algorithm>

unsigned longestIncrementingSubsequence(std::list<int> nums)
{
    std::map<int, unsigned> map{};
    unsigned max = 0;
    
    for (int num : nums)
    {
        if (map.count(num - 1))
            map[num] = std::max(map[num], map[num - 1] + 1);
        else
            map[num] = std::max(map[num], 1u);
        max = std::max(max, map[num]);
    }
    
    return max;
}

unsigned longestIncreasingSubsequence(std::vector<int> nums)
{
    unsigned maxes[nums.size()];
    unsigned max = 0;
    
    for (unsigned i = 0; i < nums.size(); i++)
    {
        maxes[i] = 1;
        for (unsigned j = 0; j < i; j++)
            if (nums[i] > nums[j])
                maxes[i] = std::max(maxes[i], maxes[j] + 1);
        max = std::max(max, maxes[i]);
    }
    
    return max;
}

int main()
{
    std::cout << "longest incrementing subsequence: "
              << longestIncrementingSubsequence({10, 9, 2, 5, 3, 7, 101, 18}) << std::endl;
    std::cout << "longest increasing subsequence: "
              << longestIncreasingSubsequence({10, 9, 2, 5, 3, 7, 101, 18}) << std::endl;

    return 0;
}
