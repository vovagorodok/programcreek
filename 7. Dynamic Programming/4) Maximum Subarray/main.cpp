#include <iostream>
#include <vector>
#include <algorithm>

int maximumSubarraySum(std::vector<int> array)
{
    int sum = array.front();
    int maxSum = sum;

    for (int el : array)
    {
        if (sum <= 0) sum = el;
        else sum += el;
        maxSum = std::max(maxSum, sum);
    }
    
    return maxSum;
}

int main()
{
    std::cout << "maximum subarray: "
              << maximumSubarraySum({-2, 1, -3, 4, -1, 2, 1, -5, 4}) << std::endl;

    return 0;
}
