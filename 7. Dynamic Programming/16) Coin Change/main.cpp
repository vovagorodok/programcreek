#include <iostream>
#include <vector>

unsigned coinChange(std::vector<unsigned> coins, unsigned amount)
{
    unsigned array[amount + 1]{};

    array[0] = 1;
    for (unsigned coin : coins)
        for (unsigned i = coin; i <= amount; i++)
            array[i] += array[i - coin];
    
    return array[amount];
}

int main()
{
    std::cout << "coin change: " << coinChange({1, 2, 3}, 4) << std::endl;
    std::cout << "coin change: " << coinChange({2, 5, 3, 6}, 10) << std::endl;

    return 0;
}
