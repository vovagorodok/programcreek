#include <iostream>
#include <string_view>
#include <set>
#include <algorithm>

bool wordBreak(std::string_view str, const std::set<std::string_view>& dict)
{
    const unsigned size = str.size() + 1;
    int pos[size];
    std::fill_n(pos, size, -1);
    pos[0] = 0;
    
    for (int begin = 0; begin < str.size(); begin++)
        if (pos[begin] != -1)
            for (int end = begin + 1; end <= str.size(); end++)
                if (dict.count(str.substr(begin, end - begin)))
                    pos[end] = begin;
    
    return pos[str.size()] != -1;
}

int main()
{
    std::set<std::string_view> dict = {"leet", "code"};
    std::string_view str = "leetcode";
    
    std::cout << "can be segmented: " << std::boolalpha << wordBreak(str, dict) << std::endl;
    
    return 0;
}
