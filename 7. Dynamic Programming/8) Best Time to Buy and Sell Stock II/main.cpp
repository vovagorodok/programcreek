#include <iostream>
#include <list>
#include <algorithm>

int maxProfitForOneTransactionInTheSameTime(std::list<int> prices)
{
    int lastPrice = prices.front();
    int maxProfit = 0;
    
    for (int currentPrice : prices)
    {
        int profit = currentPrice - lastPrice;
        if (profit > 0) maxProfit += profit;
        lastPrice = currentPrice;
    }
    
    return maxProfit;
}

int main()
{
    std::cout << "max profit for one transaction in the same time: "
              << maxProfitForOneTransactionInTheSameTime({5, 1, 2, 3, 4}) << std::endl;
    
    return 0;
}
