#include <iostream>
#include <vector>
#include <algorithm>

int maximumSubarrayProd(std::vector<int> array)
{
    int maxProd = array.front();
    int min = maxProd;
    int max = maxProd;

    for (int i = 1; i < array.size(); i++)
    {
        int mun = array[i];
        
        if (mun <= 0) std::swap(min, max);
        
        min = std::min(mun, mun * min);
        max = std::max(mun, mun * max);
        maxProd = std::max(maxProd, max);
    }
    
    return maxProd;
}

int main()
{
    std::cout << "maximum subarray: "
              << maximumSubarrayProd({2, 3, -2, 4}) << std::endl;

    return 0;
}
