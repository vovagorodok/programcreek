#include <iostream>
#include <string_view>
#include <set>
#include <list>
#include <vector>

using Sentence = std::list<std::string_view>;
using Sentences = std::list<Sentence>;

Sentences genSentences(Sentence sentence,
                       const std::vector<std::list<std::string_view>>& pos,
                       unsigned endPos)
{
    Sentences ret{};
    
    for (std::string_view word : pos[endPos])
    {
        unsigned beginPos = endPos + 1 - word.size();
        Sentence concat = sentence;
        concat.push_front(word);

        if (beginPos == 0)
            ret.push_back(std::move(concat));
        else
            ret.splice(ret.end(),
                       genSentences(std::move(concat), pos, beginPos - 1));
    }
    
    return ret;
}

Sentences wordBreak2(std::string_view str, const std::set<std::string_view>& dict)
{
    std::vector<std::list<std::string_view>> pos(str.size());

    for (unsigned begin = 0; begin < str.size(); begin++)
        if (begin == 0 or not pos[begin - 1].empty())
            for (unsigned end = begin; end < str.size(); end++)
            {
                std::string_view substr = str.substr(begin, end - begin + 1);
                if (dict.count(substr))
                    pos[end].push_back(substr);
            }

    return genSentences({}, pos, pos.size() - 1);
}

int main()
{
    std::set<std::string_view> dict = {"cat", "cats", "and", "sand", "dog"};
    std::string_view str = "catsanddog";
    
    std::cout << "segmented sentences:" << std::endl;
    for (Sentence& sentence : wordBreak2(str, dict))
    {
        for (std::string_view word : sentence)
            std::cout << word << " ";
        std::cout << std::endl;
    }
    
    return 0;
}
