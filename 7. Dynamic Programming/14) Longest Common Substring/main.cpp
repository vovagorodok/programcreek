#include <iostream>
#include <string_view>

unsigned longestCommonSubstring(std::string_view str1, std::string_view str2)
{
    unsigned dp0[str2.size()]{};
    unsigned dp1[str2.size()]{};
    auto* before = dp0;
    auto* current = dp1;

    unsigned max = 0;
        
    for (unsigned i1 = 0; i1 < str1.size(); i1++)
    {
        std::swap(current, before);
        for (unsigned i2 = 0; i2 < str2.size(); i2++)
        {
            if (str1[i1] == str2[i2])
            {
                current[i2] = 1;
                if (i2 > 0) current[i2] += before[i2 - 1];
                max = std::max(max, current[i2]);
            }
            else current[i2] = 0;
        }
    }
    
    return max;
}

int main()
{
    std::cout << "longest common substring: "
              << longestCommonSubstring("GeeksforGeeks", "GeeksQuiz") << std::endl;
    std::cout << "longest common substring: "
              << longestCommonSubstring("abcdxyz", "xyzabcd") << std::endl;
    std::cout << "longest common substring: "
              << longestCommonSubstring("zxabcdezy", "yzabcdezx") << std::endl;

    return 0;
}
