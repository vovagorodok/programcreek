#include <iostream>
#include <algorithm>
#include <vector>

unsigned maxRobSum(std::vector<unsigned>& nums, unsigned begin, unsigned size)
{
    unsigned maxNotAdjacent = size > 0 ? nums[begin] : 0;
    unsigned maxAdjacent = size > 1 ? std::max(nums[begin], nums[begin + 1]) : maxNotAdjacent;
    unsigned maxCurrent = size > 2 ? 0 : maxAdjacent;
    
    for (unsigned i = begin + 2; i < begin + size; i++)
    {
        maxCurrent = std::max(maxNotAdjacent + nums[i], maxAdjacent);
        maxNotAdjacent = maxAdjacent;
        maxAdjacent = maxCurrent;
    }

    return maxCurrent;
}

unsigned maxRobSum2(std::vector<unsigned> nums)
{
    if (nums.size() == 0) return 0;
    if (nums.size() == 1) return nums[0];

    return std::max(maxRobSum(nums, 0, nums.size() - 1),
                    maxRobSum(nums, 1, nums.size() - 1));
}

int main()
{
    std::cout << "max rob sum: " << maxRobSum2({50, 7, 1, 3, 8, 2, 60}) << std::endl;
    
    return 0;
}
