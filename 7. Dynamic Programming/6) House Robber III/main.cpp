#include <iostream>
#include <memory>
#include <algorithm>

struct Node
{
    unsigned data;
    std::shared_ptr<Node> left, right;
    Node(int data) : data(data), left(nullptr), right(nullptr) {}
}; 

struct Result
{
    unsigned notRobbed;
    unsigned robbed;
    unsigned max() { return std::max(notRobbed, robbed); }
};

Result maxRobSum3(std::shared_ptr<Node> node)
{
    if (not node) return {0, 0};

    Result left = maxRobSum3(node->left);
    Result right = maxRobSum3(node->right);
    
    return {left.max() + right.max(),
            node->data + left.notRobbed + right.notRobbed};
}

int main() 
{ 
    auto root = std::make_shared<Node>(1);
    root->left = std::make_shared<Node>(2);
    root->right = std::make_shared<Node>(4);
    root->left->left = std::make_shared<Node>(4);
    root->left->right = std::make_shared<Node>(3);

    std::cout << "max rob sum: " << maxRobSum3(root).max() << std::endl;

    return 0; 
}
