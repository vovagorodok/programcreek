#include <iostream>
#include <string_view>

std::string_view maxSize(std::string_view str1, std::string_view str2)
{
    return str1.size() >= str2.size() ? str1 : str2;
}

std::string_view longestPalindrome(std::string_view str, int begin, int end)
{
    while (begin >= 0 and end < str.size() and str[begin] == str[end])
    {
        begin--;
        end++;
    }
    begin++;
    return str.substr(begin, end - begin);
}

std::string_view longestPalindrome(std::string_view str)
{
    std::string_view longest{};
    for (int i = 0; i < str.size(); i++)
    {
        longest = maxSize(longest, longestPalindrome(str, i, i));
        longest = maxSize(longest, longestPalindrome(str, i, i + 1));
    }
    return longest;
}

int main()
{
    std::cout << "longest palindrome: " << longestPalindrome("addvfgasdffdsav") << std::endl;
    
    return 0;
}
