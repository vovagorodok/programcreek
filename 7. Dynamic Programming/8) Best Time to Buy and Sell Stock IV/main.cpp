#include <iostream>
#include <vector>
#include <algorithm>

int maxProfitForKTransactions(int k, std::vector<int> prices)
{
    std::vector<int> local(k + 1);
    std::vector<int> global(k + 1);
    
    for (int i =0; i < prices.size() - 1; i++)
    {
        int diff = prices[i + 1] - prices[i];
        for (int j = k; j >= 1; j--)
        {
            local[j] = std::max(global[j - 1] + std::max(diff, 0), local[j] + diff);
			global[j] = std::max(local[j], global[j]);
        }
    }
    
    return global[k];
}

int main()
{
    std::cout << "max profit for k transactions: "
              << maxProfitForKTransactions(2, {1, 4, 5, 7, 6, 3, 2, 9}) << std::endl;
    
    return 0;
}
