#include <iostream>
#include <string_view>
#include <vector>
#include <algorithm>
#include <limits>

void expandAround(std::string_view str, std::vector<unsigned>& dp, int i, int j)
{
    while (i >= 0 and j < str.size() and str[i] == str[j])
    {
        dp[j] = i == 0 ? 0 : std::min(dp[j], dp[i - 1] + 1);
        i--; j++;
    }
}

unsigned minPalidromsCuts(std::string_view str)
{
    std::vector<unsigned> dp(str.size());
    std::fill_n(dp.begin(), str.size(), std::numeric_limits<unsigned>::max());
    dp[0] = 0;
    
    for (int i = 1; i < str.size(); i++)
    {
        expandAround(str, dp, i, i);
        expandAround(str, dp, i, i + 1);
    }
    
    return dp[str.size() - 1];
}

int main()
{
    std::cout << "min palidroms cuts: " << minPalidromsCuts("faddarkzz") << std::endl;
    
    return 0;
}
