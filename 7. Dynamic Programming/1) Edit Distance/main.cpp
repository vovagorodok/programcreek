#include <iostream>
#include <cmath>
#include <string_view>

unsigned minDistance(std::string_view word1, std::string_view word2)
{
    unsigned dp[word1.size() + 1][word2.size() + 1];
    for (unsigned i = 0; i <= word1.size(); i++) dp[i][0] = i;
    for (unsigned j = 0; j <= word2.size(); j++) dp[0][j] = j;
    
    for (unsigned i = 0; i < word1.size(); i++)
    {
        auto char1 = word1[i];
        for (unsigned j = 0; j < word2.size(); j++)
        {
            auto char2 = word2[j];
            
            if (char1 == char2) dp[i + 1][j + 1] = dp[i][j];
            else
            {
                unsigned rep = dp[i][j] + 1;
                unsigned ins = dp[i][j + 1] + 1;
                unsigned del = dp[i + 1][j] + 1;
                
                dp[i + 1][j + 1] = std::min(std::min(rep, ins), del);
            }
        }
    }
    
    return dp[word1.size()][word2.size()];
}

int main()
{
    std::cout << "min distance: " << minDistance("abc", "adefg") << std::endl;
    
    return 0;
}
