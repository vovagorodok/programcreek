#include <iostream>
#include <string_view>

bool canBeTwoDigits(std::string_view encodedStr, unsigned begin)
{
    return begin + 1 != encodedStr.size() and
           (encodedStr[begin] == '1' or (encodedStr[begin] == '2' and encodedStr[begin + 1] < '7'));
}

unsigned numOfDecodings(std::string_view encodedStr, unsigned begin = 0)
{
    if (begin == encodedStr.size()) return 1;
    
    unsigned count = numOfDecodings(encodedStr, begin + 1);

    if (canBeTwoDigits(encodedStr, begin))
        count += numOfDecodings(encodedStr, begin + 2);
        
    return count;
}

int main()
{
    std::cout << "num of decodings: " << numOfDecodings("121") << std::endl;
    std::cout << "num of decodings: " << numOfDecodings("1234") << std::endl;
    
    return 0;
}
