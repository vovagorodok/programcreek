#include <iostream>
#include <string_view>

unsigned longestCommonSubsequence(std::string_view str1, std::string_view str2)
{
    unsigned array[str2.size()]{};
        
    for (unsigned i1 = 0; i1 < str1.size(); i1++)
        for (unsigned i2 = 0; i2 < str2.size(); i2++)
        {
            if (str1[i1] == str2[i2])
            {
                array[i2] = 1;
                if (i2 > 0)
                    array[i2] += array[i2 - 1];
            }
            else if (i2 > 0)
                array[i2] = std::max(array[i2], array[i2 - 1]);
        }
    
    return array[str2.size() - 1];
}

int main()
{
    std::cout << "longest common subsequence: "
              << longestCommonSubsequence("ABCDGH", "AEDFHR") << std::endl;
    std::cout << "longest common subsequence: "
              << longestCommonSubsequence("AGGTAB", "GXTXAYB") << std::endl;

    return 0;
}
