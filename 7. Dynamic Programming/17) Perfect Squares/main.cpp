#include <iostream>
#include <cmath>
#include <algorithm>
#include <limits>

unsigned numSquares(unsigned n)
{
    unsigned maxSqrt = static_cast<unsigned>(std::sqrt(n));
    unsigned array[n + 1];
    std::fill_n(array, n + 1,  std::numeric_limits<unsigned>::max());
    
    for (unsigned i = 1; i <= n; i++)
        for (unsigned sqrt = 1; sqrt <= maxSqrt; sqrt++)
        {
            unsigned square = sqrt * sqrt;
            if (i == square)
                array[i] = 1;
            else if (i > square)
                array[i] = std::min(array[i], array[i - square] + 1);
        }

    return array[n];
}

int main()
{
    std::cout << "num squares: " << numSquares(12) << std::endl;
    std::cout << "num squares: " << numSquares(13) << std::endl;

    return 0;
}
