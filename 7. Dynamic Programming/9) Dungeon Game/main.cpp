#include <iostream>
#include <vector>
#include <limits>

using Matrix = std::vector<std::vector<int>>;
constexpr int UnreachableCost = std::numeric_limits<int>::min();

int cellEnterCost(int sumCost, int cellCost)
{
    if (sumCost == UnreachableCost) return UnreachableCost;
    
    int enterCost = sumCost + cellCost;
    return enterCost >= 0 ? UnreachableCost : enterCost;
}

int dungeonGame(const Matrix& dungeon)
{
    unsigned m = dungeon.size();
    unsigned n = dungeon.front().size();
    int dp[m][n];
    
    dp[0][0] = dungeon[0][0];
    for (int i = 1; i < m; i++)
        dp[i][0] = cellEnterCost(dp[i - 1][0], dungeon[i][0]);
    for (int j = 1; j < n; j++)
        dp[0][j] = cellEnterCost(dp[0][j - 1], dungeon[0][j]);
    for (int i = 1; i < m; i++)
        for (int j = 1; j < n; j++)
            dp[i][j] = std::max(cellEnterCost(dp[i - 1][j], dungeon[i][j]),
                                cellEnterCost(dp[i][j - 1], dungeon[i][j]));

    int enterCost = dp[m - 1][n - 1];
    return enterCost == UnreachableCost ? UnreachableCost : 1 - enterCost;
}

int main()
{
    Matrix dungeon =
        {{-2, -3,  3},
         {-5, 10,  1},
         {10, 30, -5}};
    
    std::cout << "dungeon game: " << dungeonGame(dungeon) << std::endl;

    return 0;
}
