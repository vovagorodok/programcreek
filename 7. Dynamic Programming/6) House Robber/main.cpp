#include <iostream>
#include <algorithm>
#include <vector>

unsigned maxRobSum(std::vector<unsigned> nums)
{
    unsigned maxNotAdjacent = nums.size() > 0 ? nums[0] : 0;
    unsigned maxAdjacent = nums.size() > 1 ? std::max(nums[0], nums[1]) : maxNotAdjacent;
    unsigned maxCurrent = nums.size() > 2 ? 0 : maxAdjacent;
    
    for (unsigned i = 2; i < nums.size(); i++)
    {
        maxCurrent = std::max(maxNotAdjacent + nums[i], maxAdjacent);
        maxNotAdjacent = maxAdjacent;
        maxAdjacent = maxCurrent;
    }

    return maxCurrent;
}

int main()
{
    std::cout << "max rob sum: " << maxRobSum({6, 7, 1, 3, 8, 2, 4}) << std::endl;
    
    return 0;
}
