#include <iostream>
#include <string_view>

unsigned numDistincts(std::string_view s, std::string_view t)
{
    unsigned dp0[t.size() + 1]{};
    unsigned dp1[t.size() + 1]{};
    auto* before = dp0;
    auto* current = dp1;
    before[0] = current[0] = 1;
    
    for (unsigned i = 1; i <= s.size(); i++)
    {
        std::swap(before, current);
        for (unsigned j = 1; j <= t.size(); j++)
        {
            if (s[i - 1] == t[j - 1])
                current[j] = before[j] + before[j - 1];
            else
                current[j] = before[j];
        }
    }
    
    return current[t.size()];
}

int main()
{
    std::cout << "num distincts: " << numDistincts("rabbbit", "rabbit") << std::endl;
    
    return 0;
}
