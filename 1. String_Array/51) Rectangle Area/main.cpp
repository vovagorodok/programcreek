#include <iostream>
#include <algorithm>

struct Point
{
    double x;
    double y;
};

struct Rectangle
{
    Point leftBottom;
    Point rightTop;
};

inline bool areNotIntersected(const Rectangle& r1, const Rectangle& r2)
{
    return r1.leftBottom.x > r2.rightTop.x or r2.leftBottom.x > r1.rightTop.x or
           r1.leftBottom.y > r2.rightTop.y or r2.leftBottom.y > r1.rightTop.y;
}

inline double calcArea(const Rectangle& r)
{
    return (r.rightTop.x - r.leftBottom.x) *
           (r.rightTop.y - r.leftBottom.y);
}

inline double calcIntersectionArea(const Rectangle& r1, const Rectangle& r2)
{
    Rectangle intersection;
    intersection.leftBottom.x = std::max(r1.leftBottom.x, r2.leftBottom.x);
    intersection.leftBottom.y = std::max(r1.leftBottom.y, r2.leftBottom.y);
    intersection.rightTop.x = std::min(r1.rightTop.x, r2.rightTop.x);
    intersection.rightTop.y = std::min(r1.rightTop.y, r2.rightTop.y);

    return calcArea(intersection);
}

double calcAreas(const Rectangle& r1, const Rectangle& r2)
{
    const double area = calcArea(r1) + calcArea(r2);
    if (areNotIntersected(r1, r2)) return area;
    return area - calcIntersectionArea(r1, r2);
}

int main()
{
    Rectangle r1 = {{2, 2}, {4, 4}};
    Rectangle r2 = {{3, 3}, {5, 5}};
    std::cout << "rectangles area: " << calcAreas(r1, r2) << std::endl;
    return 0;
}
