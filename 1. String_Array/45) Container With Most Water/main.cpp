#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>

std::pair<unsigned, unsigned> findContainerWithMostWater(std::vector<unsigned> container)
{
    std::pair<unsigned, unsigned> pair{};
    unsigned max = 0;
    unsigned area, maxRight;
    unsigned left = 0;
    unsigned right = left + 1;
    
    while (left < container.size() and right < container.size())
    {
        maxRight = 0;
        do
        {
            if (container[right] > maxRight)
            {
                maxRight = container[right];
                area = std::min(container[left], container[right]) * (right - left);

                if (area > max)
                {
                    max = area;
                    pair = {left, right};
                }
            }
            right++;
        } while (right < container.size() and container[left] > maxRight);
        
        left = right - 1;
    }

    return pair;
}

int main()
{
    auto pair = findContainerWithMostWater({1, 2, 4, 2, 3, 2, 5, 2});
    std::cout << "container with most water: "
              << pair.first << ", "
              << pair.second << std::endl;
    return 0;
}
