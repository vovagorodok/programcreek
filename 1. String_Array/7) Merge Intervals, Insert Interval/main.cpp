#include <iostream>
#include <vector>
#include <algorithm>

struct Interval
{
    int begin;
    int end;
};

void printIntervals(const std::vector<Interval>& intervals)
{
    std::cout << "{";
    for (const auto& interval : intervals)
        std::cout << "[" << interval.begin << "," << interval.end << "]";
    std::cout << "}" << std::endl;
}

void sortIntervals(std::vector<Interval>& intervals)
{
    std::sort(intervals.begin(), intervals.end(),
        [](const Interval& interval1, const Interval& interval2) {
            return interval1.begin < interval2.begin;
        });
}

inline bool canBeMerged(const Interval& interval1, const Interval& interval2)
{
    return interval1.end >= interval2.begin;
}

std::vector<Interval> mergeIntervals(std::vector<Interval>& intervals)
{
    std::vector<Interval> merged{};
    sortIntervals(intervals);
    
    if (!intervals.size()) return {};
    
    merged.push_back(intervals.front());
    Interval& mergedInterval = merged.back();
    
    for (const auto& interval : intervals)
    {
        if (canBeMerged(mergedInterval, interval))
        {
            mergedInterval.end = std::max(mergedInterval.end, interval.end);
        }
        else
        {
            merged.push_back(interval);
            mergedInterval = merged.back();
        }
    }
    
    return merged;
}

int main()
{
    std::vector<Interval> intervals = {{1,3},{2,6},{8,10},{15,18}};
    printIntervals(intervals);
    printIntervals(mergeIntervals(intervals));

    return 0;
}
