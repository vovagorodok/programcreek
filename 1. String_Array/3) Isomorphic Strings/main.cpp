#include <iostream>
#include <string>
#include <map>

bool areIsomorphic(const std::string& str1, const std::string& str2)
{
    if (str1.size() != str2.size()) return false;
    
    std::map<std::string::value_type, std::string::value_type> map;
    for (std::size_t i = 0; i < str1.size(); i++)
    {
        const auto& char1 = str1[i];
        const auto& char2 = str2[i];
        
        const auto& found = map.find(char1);
        if (found != map.end())
        {
            if (found->second != char2) return false;
        }
        else map[char1] = char2;
    }
    
    return true;
}

int main()
{
    std::string str1 = "aad";
    std::string str2 = "ssh";
    
    std::cout << "strings are isomorphic: " << std::boolalpha << areIsomorphic(str1, str2) << std::endl;
    return 0;
}
