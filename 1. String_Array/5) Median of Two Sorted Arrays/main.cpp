#include <iostream>
#include <vector>
#include <algorithm>
#include <limits>

void showMedianSortedArrays(const std::vector<int>& arr1, const std::vector<int>& arr2)
{
    std::vector<int> concat;
    concat.reserve(arr1.size() + arr2.size());
    concat.insert(concat.end(), arr1.begin(), arr1.end());
    concat.insert(concat.end(), arr2.begin(), arr2.end());
    std::sort(concat.begin(), concat.end());
    std::cout << "median is: ";
    if (concat.size() % 2) std::cout << concat[concat.size() / 2];
    else std::cout << concat[concat.size() / 2 - 1] << ", " << concat[concat.size() / 2];
    std::cout << std::endl;
}

int findRecursively(unsigned k, const std::vector<int>& arr1, const std::vector<int>& arr2, unsigned s1, unsigned s2)
{
    if (s1 >= arr1.size())
        return arr2[s2 + k - 1];
 
    if (s2 >= arr2.size())
        return arr1[s1 + k - 1];
 
    if (k == 1)
        return std::min(arr1[s1], arr2[s2]);
        
    unsigned m1 = s1 + k / 2 - 1;
    unsigned m2 = s2 + k / 2 - 1;
 
    int mid1 = m1 < arr1.size() ? arr1[m1] : std::numeric_limits<int>::max();    
    int mid2 = m2 < arr2.size() ? arr2[m2] : std::numeric_limits<int>::max();
 
    if (mid1 < mid2)
        return findRecursively(k - k / 2, arr1, arr2, m1 + 1, s2);
    else
        return findRecursively(k - k / 2, arr1, arr2, s1, m2 + 1);
}

double findMedianSortedArrays(const std::vector<int>& arr1, const std::vector<int>& arr2) {
    unsigned total = arr1.size() + arr2.size();
    if (total % 2 == 0)
        return (findRecursively(total / 2 + 1, arr1, arr2, 0, 0) +
                findRecursively(total / 2,     arr1, arr2, 0, 0)) / 2.0;
    else
        return findRecursively(total / 2 + 1, arr1, arr2, 0, 0);
}

int main()
{
    std::vector<int> arr1 = {1, 2, 4, 6, 7};
    std::vector<int> arr2 = {1, 2, 5, 6, 7};
    showMedianSortedArrays(arr1, arr2);
    
    std::cout << "median is: " << findMedianSortedArrays(arr1, arr2) << std::endl;
    
    return 0;
}
