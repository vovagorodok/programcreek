#include <iostream>
#include <string_view>
#include <vector>
#include <set>
#include <string>

inline void addResult(std::string_view str, std::vector<bool>& removed, std::set<std::string>& result)
{
    std::string res{};
    for (unsigned i = 0; i < str.size(); i++)
        if (not removed[i]) res += str[i];
        
    result.insert(res);
}

inline bool isValid(std::string_view str, std::vector<bool>& removed)
{
    unsigned openedToRemove = 0;

    for (unsigned i = 0; i < str.size(); i++)
    {
        if (removed[i]) continue;

        if (str[i] == '(')
            openedToRemove++;
        else if (str[i] == ')')
        {
            if (openedToRemove == 0) return false;
            openedToRemove--;
        }
    }
    
    return true;
}

void removeRecursively(std::string_view str,
                       std::vector<bool>& removed,
                       std::set<std::string>& result,
                       unsigned openedToRemove,
                       unsigned closedToRemove)
{
    if (openedToRemove == 0 and closedToRemove == 0 and isValid(str, removed))
    {
        addResult(str, removed, result);
        return;
    }
        
    for (unsigned i = 0; i < str.size(); i++)
    {
        if (removed[i]) continue;

        if (str[i] == '(' and openedToRemove)
        {
            removed[i] = true;
            removeRecursively(str, removed, result, openedToRemove - 1, closedToRemove);
            removed[i] = false;
        }
        if (str[i] == ')' and closedToRemove)
        {
            removed[i] = true;
            removeRecursively(str, removed, result, openedToRemove, closedToRemove - 1);
            removed[i] = false;
        }
    }
}

std::set<std::string> removeInvalidParentheses(std::string_view str)
{
    std::vector<bool> removed(str.size());
    std::set<std::string> result{};
    unsigned openedToRemove = 0;
    unsigned closedToRemove = 0;
    
    for (auto ch : str)
    {
        if (ch == '(')
            openedToRemove++;
        else if (ch == ')')
        {
            if (openedToRemove == 0)
                closedToRemove++;
            else
                openedToRemove--;
        }
    }

    removeRecursively(str, removed, result, openedToRemove, closedToRemove);
    
    return result;
}

int main()
{
    for (auto res : removeInvalidParentheses("(a)())()"))
        std::cout << res << std::endl;

    return 0;
}
