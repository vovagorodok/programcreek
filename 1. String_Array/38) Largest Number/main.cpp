#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>

std::string maxNumberFromNumbers(std::vector<unsigned> numbers)
{
    std::vector<std::string> strings{};
    std::transform(numbers.begin(), numbers.end(), std::back_inserter(strings),
        [](unsigned number) { return std::to_string(number); });
    
    std::sort(strings.begin(), strings.end(),
        [](std::string& a, std::string& b) { return a + b > b + a; });

    return std::accumulate(strings.begin(), strings.end(), std::string(),
        [](std::string& a, std::string& b) { return a + b; });
}

int main()
{
    std::cout << "max number from numbers: " << maxNumberFromNumbers({3, 30, 34, 5, 9}) << std::endl;
    return 0;
}
