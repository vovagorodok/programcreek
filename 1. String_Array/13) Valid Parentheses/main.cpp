#include <iostream>
#include <map>
#include <stack>

bool areValidParentheses(const std::string& str)
{
    const std::map<std::string::value_type, std::string::value_type> pairs =
    {
        {']', '['},
        {')', '('},
        {'}', '{'}
    };
    std::stack<std::string::value_type> stack{};
    
    for (auto parenthes : str)
    {
        switch (parenthes)
        {
            case '[':
            case '(':
            case '{':
                stack.push(parenthes);
                break;
            case ']':
            case ')':
            case '}':
                if (stack.top() == pairs.at(parenthes))
                    stack.pop();
                else return false;
                break;
            default:
                return false;
        }
    }
    return stack.empty();
}

int main()
{
    std::string str = {"[](([{}]))"};
    std::cout << "parenthneses are valid: " << std::boolalpha << areValidParentheses(str) << std::endl;
    
    return 0;
}
