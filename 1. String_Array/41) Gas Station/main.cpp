#include <iostream>
#include <vector>

int getIndexForStart(std::vector<unsigned> gas, std::vector<unsigned> cost)
{
    unsigned start = 0;
    int total = 0;
    int sumRamaning = 0;

    for (unsigned i = 0; i < gas.size(); i++)
    {
        int remaning = gas[i] - cost[i];
        
        if (sumRamaning >= 0)
            sumRamaning += remaning;
        else
        {
            sumRamaning = remaning;
            start = i;
        }
        
        total += remaning;
    }
    
    return total >= 0 ? start : -1;
}

int main()
{
    std::cout << "index for start: "
              << getIndexForStart({1, 2, 3, 4, 5},
                                  {1, 2, 3, 4, 5})
              << std::endl;
              
    return 0;
}
