#include <iostream>
#include <string>
#include <string_view>
#include <stack>

inline std::string generateNewPath(std::stack<std::string_view>& stack, bool isAbsolutePath)
{
    if (stack.empty() and isAbsolutePath) return "/";
    
    std::string ret{};
    while (not stack.empty())
    {
        std::string str(stack.top());
        if (stack.size() == 1 and not isAbsolutePath)
            ret = str + ret;
        else
            ret = "/" + str + ret;
        stack.pop();
    }
    return ret;
}

inline bool startsWith(std::string_view view, std::string_view prefix)
{
    return view.size() < prefix.size() ? false :
        view.substr(0, prefix.size()) == prefix;
}

inline void handleStr(std::string_view str, std::stack<std::string_view>& stack, bool isAbsolutePath)
{
    if (str == "..")
    {
        if (not stack.empty())
        {
            if (stack.top() == "..") stack.push(str);
            else stack.pop();
        }
        else if (not isAbsolutePath) stack.push(str);
    }
    else if (not (str == ".")) stack.push(str);
}

std::string simplifyPath(std::string_view path)
{
    bool isAbsolutePath = startsWith(path, "/");
    std::stack<std::string_view> stack{};
    
    std::size_t currentPos = 0;
    std::size_t foundPos = 0;

    while (currentPos != path.size())
    {
        foundPos = path.find_first_of("/", currentPos);
        if (foundPos != currentPos)
        {
            auto str = path.substr(currentPos, foundPos - currentPos);
            handleStr(str, stack, isAbsolutePath);
        }
        currentPos = foundPos + 1;
    }

    return generateNewPath(stack, isAbsolutePath);
}

int main()
{
    auto paths = {"/home/", "/a/./b/../../c/", "/../", "/home//foo/", "a/../../"};

    for (auto& path : paths)
        std::cout << path << " => "<< simplifyPath(path) << std::endl;

    return 0;
}
