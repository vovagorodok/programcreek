#include <iostream>
#include <vector>
#include <list>

std::list<int> isReachableRecursively(const std::vector<unsigned>& numbers, unsigned left, unsigned right)
{
    std::list<int> results{};
    if (left == right)
    {
        results.push_back(numbers[left]);
        return results;
    }
    
    for (unsigned i = left; i < right; i++)
    {
        std::list<int> leftResults = isReachableRecursively(numbers, left, i);
        std::list<int> rightResults = isReachableRecursively(numbers, i + 1, right);
        
        for (int leftResult : leftResults)
            for (int rightResult : rightResults)
            {
                results.push_back(leftResult + rightResult);
                results.push_back(leftResult - rightResult);
                results.push_back(leftResult * rightResult);
                if (rightResult) results.push_back(leftResult / rightResult);
            }
    }
    
    return results;
}

bool isReachable(std::vector<unsigned> numbers, int target)
{
    if (not numbers.size()) return false;
    
    std::list<int> results = isReachableRecursively(numbers, 0, numbers.size() - 1);
    for (int result : results)
        if (result == target) return true;
    
    return false;
}

int main()
{
    std::cout << "target is reachable: " << std::boolalpha
              << isReachable({1, 2, 3, 4}, 21) << std::endl;
    return 0;
}
