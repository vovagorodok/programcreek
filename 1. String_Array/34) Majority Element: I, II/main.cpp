#include <iostream>
#include <vector>

int majorityElement(const std::vector<int> nums)
{
    int result = 0, count = 0;
 
    for(int num : nums)
    {
        if (count == 0 or result == num)
        {
            result = num;
            count++;
        }
        else count--;
    }
 
    return result;
}

int main()
{
    std::cout << "majority element: " << majorityElement(
        {2, 4, 5, 2, 2, 4, 5, 2, 2, 4, 2});
    return 0;
}
