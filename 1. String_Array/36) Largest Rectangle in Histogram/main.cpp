#include <iostream>
#include <vector>
#include <stack>
#include <algorithm>

int maxAreaInHistogram(const std::vector<int> histogram)
{
    std::stack<int> stack{};
    int maxArea = 0;
    int i = 0;
    
    while (i < histogram.size())
    {
        if (stack.empty() or histogram[stack.top()] <= histogram[i])
        {
            stack.push(i);
            i++;
        }
        else
        {
            const int width = i - stack.top();
            const int high = histogram[stack.top()];
            const int area = width * high;
            maxArea = std::max(maxArea, area);
            stack.pop();
        }
    }
    
    while (!stack.empty())
    {
        const int width = i - stack.top();
        const int high = histogram[stack.top()];
        const int area = width * high;
        maxArea = std::max(maxArea, area);
        stack.pop();
    }
 
    return maxArea;
}

int main()
{
    std::cout << "max area: " << maxAreaInHistogram({2, 1, 5, 6, 2, 3}) << std::endl;
    return 0;
}
