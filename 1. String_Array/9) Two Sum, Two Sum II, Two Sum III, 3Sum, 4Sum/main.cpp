#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <set>

template <typename Container>
inline void printContainer(const Container& container)
{
    std::cout << "[";
    for (const auto& el : container)
        std::cout << el << ", ";
    std::cout << "]" << std::endl;
}

void prepareArray(std::vector<int>& arr)
{
    std::sort(arr.begin(), arr.end());
    arr.erase(std::unique(arr.begin(), arr.end()), arr.end());
}

int sum(const std::vector<int>& arr, const std::vector<unsigned>& indexes)
{
    // use std::accumulate instead
    int sum = 0;
    for (unsigned id : indexes) sum += arr[id];
    return sum;
}

struct Params
{
    unsigned x;
    std::vector<unsigned>& indexes;
    const std::vector<int>& arr;
    int expectedSum;
};

inline bool isMaxDepthReached(const Params& params, unsigned index)
{
    return index == params.x - 1;
}

inline unsigned& currentIndex(const Params& params, unsigned index)
{
    return params.indexes[index];
}

inline unsigned& nextIndex(const Params& params, unsigned index)
{
    return params.indexes[index + 1];
}

inline unsigned maxIndexValue(const Params& params, unsigned index)
{
    return params.arr.size() - params.x + index;
}

void showSolution(Params& params)
{
    std::cout << "solution: [";
    for (const auto& id : params.indexes)
        std::cout << params.arr[id] << ", ";
    std::cout << "]" << std::endl;
}

inline void verifySum(Params& params)
{
    if (sum(params.arr, params.indexes) == params.expectedSum)
    {
        showSolution(params);
    }
}

void findSumRecursively(Params& params, unsigned index)
{
    for (unsigned& i = currentIndex(params, index); i <= maxIndexValue(params, index); i++)
    {
        if (isMaxDepthReached(params, index))
        {
            verifySum(params);
        }
        else
        {
            nextIndex(params, index) = currentIndex(params, index) + 1;
            findSumRecursively(params, index + 1);
        }
    }
}

void findSumOf2(std::vector<int>& arr, int expectedSum)
{
    std::set<int> set(arr.begin(), arr.end());
    for (int el : arr)
    {
        if (expectedSum - el > el and set.count(expectedSum - el))
            std::cout << "solution: [" << el << ", " << expectedSum - el<< "]" << std::endl;
    }
}

void findSumOfX(unsigned x, std::vector<int>& arr, int expectedSum)
{
    if (arr.size() < x) return;
    if (x == 2)
    {
        findSumOf2(arr, expectedSum);
        return;
    }
    
    prepareArray(arr);
    std::vector<unsigned> indexes(x);
    Params params{x, indexes, arr, expectedSum};
    
    findSumRecursively(params, 0);
}

int main()
{
    std::vector<int> arr = {1, 0, -1, 0, -2, 2};
    findSumOfX(3, arr, 0);
    printContainer(arr);

    return 0;
}
