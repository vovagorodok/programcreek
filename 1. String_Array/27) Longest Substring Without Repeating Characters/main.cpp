#include <iostream>
#include <string>
#include <map>

void printSubstring(const std::string& str, unsigned start, unsigned end)
{
    for (unsigned i = start; i < end; i++)
        std::cout << str[i];
    std::cout << std::endl;
}

inline unsigned longestSubstringWithoutKRepeating(unsigned k, const std::string& str)
{
    std::map<std::string::value_type, unsigned> map{};
    unsigned start = 0;
    unsigned end = 0;
    unsigned longest = 0;
    bool found = false;

    while (start < str.size() and end < str.size())
    {
        found = false;
        while (end < str.size() and not found)
        {
            map[str[end]]++;
            found = map[str[end]] == k;
            end++;
        }

        unsigned lastRepeated = found ? end - 1 : end;
        if (longest < lastRepeated - start)
        {
            longest = lastRepeated - start;
            std::cout << "candidate: " << start << ", " << lastRepeated << std::endl;
            printSubstring(str, start, lastRepeated);
        }
        
        found = false;
        while (start < str.size() and not found)
        {
            found = map[str[start]] == k;
            map[str[start]]--;
            start++;
        }
    }
    
    return longest;
}

int main()
{
    std::string str = {"abcabcbb"};
    std::cout << "longest: " << longestSubstringWithoutKRepeating(2, str) << std::endl;
    
    return 0;
}
