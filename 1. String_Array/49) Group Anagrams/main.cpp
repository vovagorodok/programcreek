#include <iostream>
#include <string_view>
#include <map>
#include <cctype>
#include <array>
#include <list>

constexpr std::size_t ARRAY_SIZE = 'z' - 'a';
using Strings = std::list<std::string_view>;
using ArrayWithCounters = std::array<unsigned, ARRAY_SIZE>;

void printAnagrams(Strings strings)
{
    std::map<ArrayWithCounters, Strings> map{};
    
    for (auto str : strings)
    {
        ArrayWithCounters arr{};
        for (auto ch : str)
        {
            auto lower = std::tolower(ch);
            if (lower >= 'a' and lower <= 'z')
                arr[lower - 'a']++;
        }
        map[arr].push_back(str);
    }
    
    std::cout << "groups of anagrams:" << std::endl;
    for (auto pair : map)
    {
        for (auto str : pair.second)
        {
            std::cout << "\"" << str << "\" ";
        }
        std::cout << std::endl;
    }
}

int main()
{
    printAnagrams({"Torchwood", "Doctor Who", "asa", "saa"});
    return 0;
}
