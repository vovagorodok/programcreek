#include <iostream>
#include <vector>
#include <utility>

inline int findLeft(const std::vector<unsigned>& container, unsigned num)
{
    int found = -1;
    unsigned k = container.size() / 2;
    unsigned left = k;
    while (k > 0)
    {
        k = k == 1 ? 0 : k - k / 2;
        if (container[left] == num) found = left;
        if (container[left] >= num) left -= k;
        else left += k;
    }

    return found;
}

inline int findRight(const std::vector<unsigned>& container, unsigned num)
{
    int found = -1;
    unsigned k = container.size() / 2;
    unsigned left = k;
    while (k > 0)
    {
        k = k == 1 ? 0 : k - k / 2;
        if (container[left] == num) found = left;
        if (container[left] <= num) left += k;
        else left -= k;
    }

    return found;
}

std::pair<int, int> findRange(std::vector<unsigned> container, unsigned num)
{
    return {findLeft(container, num), findRight(container, num)};
}

int main()
{
    auto pair = findRange({5, 7, 7, 8, 8, 10}, 8);
    std::cout << "found range: "
              << pair.first << ", "
              << pair.second << std::endl;
    return 0;
}
