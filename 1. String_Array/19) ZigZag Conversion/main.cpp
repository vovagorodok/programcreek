#include <iostream>
#include <string>

inline std::string convertZigZagRow(const std::string& str, unsigned numRows, unsigned row)
{
    std::string ret{};    
    const unsigned mumOfCharsUsedInPeriod = numRows * 2 - 2;
    const unsigned numOfPeriods = str.size() / mumOfCharsUsedInPeriod + 1;
    
    for (unsigned period = 0; period < numOfPeriods; period++)
    {
        const unsigned firstCharInPeriod = period * mumOfCharsUsedInPeriod;
        const unsigned firstCharInRow = firstCharInPeriod + row;
        if (firstCharInRow < str.size()) ret.push_back(str.at(firstCharInRow));
        
        for (unsigned offset = 1; offset < numRows - 1; offset++)
        {
            const unsigned offsetToShow = numRows - row - 1;
            const unsigned offsetInStr = numRows + offset - 1;
            const unsigned charNumberInStr = firstCharInPeriod + offsetInStr;
            if (offset == offsetToShow and charNumberInStr < str.size())
            {
                ret.push_back(str.at(charNumberInStr));
            }
            else ret.push_back(' ');
        }
    }
    ret.push_back('\n');
    return ret;
}

inline std::string convertZigZag(const std::string& str, unsigned numRows)
{
    if (numRows == 1) return str;
    
    std::string ret{};
    for (unsigned row = 0; row < numRows; row++)
        ret.append(convertZigZagRow(str, numRows, row));
    return ret;
}

int main()
{
    std::string str = {"ABCDEFGHIJKLMNOPQRST"};
    std::cout << convertZigZag(str, 6) << std::endl;
    
    return 0;
}
