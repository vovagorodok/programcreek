#include <iostream>
#include <list>
#include <vector>
#include <string>

void printTtiangle(std::list<std::vector<unsigned>> triangle)
{
    for (auto& row : triangle)
    {
        std::cout << std::string(triangle.size() - row.size(), ' ');
        for (auto& el : row)
            std::cout << el << ' ';
        std::cout << std::endl;
    }
}

std::list<std::vector<unsigned>> generatePascalTriangle(unsigned num)
{
    std::list<std::vector<unsigned>> triangle{};
    if (num > 0) triangle.push_back({1});
    if (num > 1) triangle.push_back({1, 1});
    
    for (unsigned row = 2; row < num; row++)
    {
        std::vector<unsigned>& rowBefore = triangle.back();
        std::vector<unsigned> newRow{};
        
        newRow.emplace_back(1);
        for (unsigned i = 0; i < rowBefore.size() -1; i++)
            newRow.emplace_back(rowBefore[i] + rowBefore[i + 1]);
        newRow.emplace_back(1);
        
        triangle.emplace_back(std::move(newRow));
    }
  
    return triangle;
}

int main()
{
    printTtiangle(generatePascalTriangle(5));
    return 0;
}
