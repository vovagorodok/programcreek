#include <iostream>
#include <string_view>

struct Result
{
    int res;
    unsigned end;
};

Result evaluateRecursively(std::string_view str, unsigned start = 0)
{
    int res = 0;
    bool minus = false;
    unsigned i = start;
    while (i < str.size())
    {
        if (str[i] == '(')
        {
            Result subres = evaluateRecursively(str, i + 1);
            res += minus ? -subres.res : subres.res;
            i = subres.end;
        }
        else if (str[i] == ')')
        {
            return {res, i};
        }
        else if (str[i] == '-')
        {
            minus = true;
        }
        else if (str[i] == '+')
        {
            minus = false;
        }
        else if (str[i] >= '0' and str[i] <= '9')
        {
            int var = str[i] - '0';
            res += minus ? -var : var;
        }
        i++;
    }
    
    return {res, i};
}

int evaluate(std::string_view str)
{
    return evaluateRecursively(str).res;
}

int main()
{
    std::cout << "result: " << evaluate("(1-(4-5)) - 1 - 3 + 4") << std::endl;
    return 0;
}
