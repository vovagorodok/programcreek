#include <iostream>
#include <vector>
#include <algorithm>

unsigned calcHIndex(std::vector<unsigned> citations)
{
    std::sort(citations.begin(), citations.end());
    
    for (unsigned i = 0; i < citations.size(); i++)
    {
        const unsigned leftCitations = citations.size() - i;
        if (citations[i] >= leftCitations) return leftCitations;
    }
    
    return 0;
}

int main()
{
    std::cout << "h index: " << calcHIndex({3, 0, 6, 1, 5}) << std::endl;
    return 0;
}
