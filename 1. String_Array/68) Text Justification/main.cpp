#include <iostream>
#include <string_view>
#include <list>

constexpr unsigned ONE_SPACE = 1;

void justify(std::list<std::string_view> words, unsigned l)
{
    unsigned charsInLine = 0;
    for (auto word : words)
    {
        if ((charsInLine + word.size() + ONE_SPACE) > 16)
        {
            std::cout << std::endl;
            charsInLine = 0;
        }
        if (charsInLine)
        {
            std::cout << " ";
            charsInLine += ONE_SPACE;
        }
        std::cout << word;
        charsInLine += word.size();
    }
}

int main()
{
    justify({"This", "is", "an", "example", "of", "text", "justification."}, 16);
    return 0;
}
