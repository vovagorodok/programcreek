#include <iostream>
#include <string_view>
#include <list>
#include <utility>
#include <cmath>

bool isOneEditDistance(std::string_view s1, std::string_view s2)
{
    if (std::abs(int(s1.size() - s2.size())) > 1) return false;
    
    bool isChangedOnce = false;
    unsigned id1 = 0, id2 = 0;

    while (id1 < s1.size() and id2 < s2.size())
    {
        if (s1[id1] == s2[id2])
        {
            id1++;
            id2++;
        }
        else
        {
            if (isChangedOnce) return false;
            isChangedOnce = true;
            
            if (s1.size() > s2.size())
                id1++;
            else if (s2.size() > s1.size())
                id2++;
            else
            {
                id1++;
                id2++;
            }
        }
    }
    
    if (id1 < s1.size() or id2 < s2.size())
        return not isChangedOnce;
    
    return isChangedOnce;
}

int main()
{
    std::list<std::pair<std::string_view, std::string_view>> stringPairs = {
        {"geeks", "geek"},
        {"geeks", "geeks"},
        {"geaks", "geeks"},
        {"peaks", "geeks"}};

    for (auto pair : stringPairs)
    {
        std::cout << "is one edit distance between '" << pair.first
                  << "' and '" << pair.second
                  << "': " << std::boolalpha
                  << isOneEditDistance(pair.first, pair.second) << std::endl;
    }
    return 0;
}
