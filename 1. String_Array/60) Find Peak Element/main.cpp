#include <iostream>
#include <vector>

unsigned findPeakElement(std::vector<int> numbers)
{
    int max = numbers[0];
    unsigned index = 0;

    for (unsigned i = 1; i < numbers.size() - 1; i++)
    {
        int prev = numbers[i-1];
        int curr = numbers[i];
        int next = numbers[i+1];

        if (curr > prev and curr > next and curr > max)
        {
            index = i;
            max = curr;
        }
    }

    if (numbers[numbers.size() - 1] > max)
        return numbers.size() - 1;

    return index;
}

int main()
{
    std::cout << "peak element: " << findPeakElement({1, 2, 3, 1}) << std::endl;
    return 0;
}
