#include <iostream>
#include <string>

bool canWin(std::string& str)
{
    for (unsigned i = 0; i < str.size() - 1; i++)
    {
        if (str[i] == '+' and str[i + 1] == '+')
        {
            str[i] = str[i + 1] = '-';
            bool win = canWin(str);
            str[i] = str[i + 1] = '+';
            
            if (not win) return true;
        }
    }
    
    return false;
}

int main()
{
    std::string flipGamePlate("+++--+-+++---++-");
    std::cout << "can win: " << std::boolalpha << canWin(flipGamePlate) << std::endl;
    
    return 0;
}
