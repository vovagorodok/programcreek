#include <iostream>
#include <utility>
#include <map>
#include <vector>
#include <algorithm>

using NumberAndFreq = std::pair<int, unsigned>;

void printTopKFrequentElements(std::vector<int> numbers, std::size_t k)
{
    std::map<int, unsigned> map{};
    for (int num : numbers) map[num]++;

    std::vector<NumberAndFreq> vec(map.begin(), map.end());
    std::sort(vec.begin(), vec.end(),
        [](const NumberAndFreq& lhs, const NumberAndFreq& rhs)
            { return lhs.second > rhs.second; });
    
    std::cout << "top " << k << " frequent elements:" << std::endl;
    for (std::size_t i = 0; i < std::min(k, vec.size()); i++)
        std::cout << vec[i].first << " ";
}

int main()
{
    printTopKFrequentElements({1, 2, 4, 4, 5, 2, 5, 6, 2, 6, 3, 2, 5, 6, 5, 5, 3, 6, 7, 3, 2}, 5);
    return 0;
}
