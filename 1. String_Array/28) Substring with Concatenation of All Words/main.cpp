#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <string>
#include <string_view>

template <class T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& vector)
{
    for (int el : vector) std::cout << el << ' ';
    return os;
}

std::vector<unsigned> findPositions(std::string_view sentence, const std::vector<std::string>& dict)
{
    const std::size_t wordSize = dict.front().size();
    const std::size_t fullDictSize = dict.size() * wordSize;
    std::vector<unsigned> result{};
    
    std::set<std::string_view> dictView(dict.begin(), dict.end());
    std::set<std::string_view> usedWords{};
    
    for (std::size_t i = 0; i < sentence.size(); i += wordSize)
    {
        auto word = sentence.substr(i, wordSize);
        if (dictView.count(word) and not usedWords.count(word))
            usedWords.insert(word);
        else
            usedWords.clear();
        
        if (usedWords.size() == dictView.size())
        {
            usedWords.clear();
            result.push_back(i + wordSize - fullDictSize);
        }
    }
    
    return result;
}

std::vector<unsigned> findPositionsFaster(std::string_view sentenceView, const std::vector<std::string>& dict)
{
    const std::size_t wordSize = dict.front().size();
    const std::size_t fullDictSize = dict.size() * wordSize;
    std::vector<unsigned> result{};
    unsigned iteration = 1;
    unsigned numberOfFounds = 0;
    
    std::map<std::string_view, unsigned> map{};
    for (std::string_view word : dict) map.insert(std::make_pair(word, 0));
    
    for (std::size_t i = 0; i < sentenceView.size(); i += wordSize)
    {
        auto word = sentenceView.substr(i, wordSize);
        auto found = map.find(word);
        
        if (found != map.end() and found->second != iteration)
        {
            found->second = iteration;
            numberOfFounds++;
        }
        else
        {
            numberOfFounds = 0;
            iteration++;
        }

        if (numberOfFounds == map.size())
        {
            numberOfFounds = 0;
            iteration++;
            result.push_back(i + wordSize - fullDictSize);
        }
    }
    
    return result;
}

int main()
{
    std::string sentence = "barfoothefoobarman";
    std::vector<std::string> dict = {"foo", "bar"};
    
    std::cout << "start positions: " << findPositions(sentence, dict);
    return 0;
}
