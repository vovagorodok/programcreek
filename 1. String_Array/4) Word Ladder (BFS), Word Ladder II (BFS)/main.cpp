#include <iostream>
#include <string>
#include <vector>
#include <stack>
#include <deque>

template <typename Container>
inline void printContainer(const Container& container)
{
    std::cout << "[";
    for (const auto& el : container)
        std::cout << el << ", ";
    std::cout << "]" << std::endl;
}

struct Params
{
    std::deque<std::string>& checked;
    const std::vector<std::string>& dict;
    const std::string& end;
    unsigned found;
};

inline unsigned numOfMismathes(const std::string& s1, const std::string& s2)
{
    unsigned num = 0;
    for (std::size_t i = 0; i < s1.length(); i++)
        if (s1[i] != s2[i]) num++;    
    return num;
}

inline bool isOneMismath(const std::string& s1, const std::string& s2)
{
    return numOfMismathes(s1, s2) == 1;
}

inline bool isMaxDepthReached(Params& params)
{
    return params.checked.size() > params.found;
}

inline bool isEndReached(Params& params)
{
    return params.checked.back() == params.end;
}

void checkRecursively(Params& params)
{
    if (isMaxDepthReached(params)) return;
    const unsigned depth = params.checked.size();
    
    for (std::size_t i = 0; i < params.dict.size(); i++)
    {
        const auto& before = params.checked.back();
        params.checked.push_back(params.dict[i]);
        const auto& after = params.checked.back();
        
        if (isOneMismath(before, after))
        {
            if (isEndReached(params))
            {
                params.found = depth;
                printContainer(params.checked);
            }
            else
            {
                checkRecursively(params);
            }
        }
        
        params.checked.pop_back();
    }
}

unsigned minStepsForWordLadder(std::string& start, std::string& end, std::vector<std::string>& dict)
{
    dict.push_back(end);
    std::deque<std::string> checked{};
    checked.push_back(start);  
    unsigned maxPosibleDepth = dict.size() + 1;    
    Params params{checked, dict, end, maxPosibleDepth};
    
    if (isEndReached(params)) return 0;
    
    checkRecursively(params); 
    if (params.found == maxPosibleDepth) std::cout << "no solution here" << std::endl;
    return params.found;
}

int main()
{
  std::string start = "hit";
  std::string end = "cog";
  std::vector<std::string> dict = {"hot","dot","dog","lot","log"};
  
  std::cout << "min steps: " << minStepsForWordLadder(start, end, dict);
}
