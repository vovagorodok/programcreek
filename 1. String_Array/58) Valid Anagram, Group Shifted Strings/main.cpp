#include <iostream>
#include <string_view>
#include <map>
#include <list>
#include <cmath>

using Strings = std::list<std::string_view>;
using StringValue = std::string_view::value_type;
constexpr StringValue DIFF_OFFSET = 'z' - 'a' + 1;

inline StringValue calcDiff(StringValue r, StringValue l)
{
    return r - l + (r < l ? DIFF_OFFSET : 0);
}

struct ShiftsComp
{
    bool operator()(std::string_view lhs, std::string_view rhs) const
    {
        if (lhs.size() != rhs.size()) return lhs.size() < rhs.size();
                
        for (std::size_t i = 0; i < lhs.size() - 1; i++)
        {
            auto lDiff = calcDiff(lhs[i + 1], lhs[i]);
            auto rDiff = calcDiff(rhs[i + 1], rhs[i]);
            
            if (lDiff != rDiff) return lDiff < rDiff;
        }
        
        return false;
    }
};

void printShifts(Strings strings)
{
    std::map<std::string_view, Strings, ShiftsComp> map{};
    
    for (auto str : strings)  map[str].push_back(str);
    
    std::cout << "groups of shifts:" << std::endl;
    for (auto pair : map)
    {
        for (auto str : pair.second)
        {
            std::cout << "\"" << str << "\" ";
        }
        std::cout << std::endl;
    }
}

int main()
{
    printShifts({"abc", "bcd", "xyf", "xyz", "az", "ba", "acef", "a", "z"});
    return 0;
}
