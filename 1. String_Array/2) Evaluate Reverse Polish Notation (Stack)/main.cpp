#include <iostream>
#include <string>
#include <vector>
#include <stack>
#include <stdexcept>

int pop(std::stack<int>& stack)
{
    int num = stack.top();
    stack.pop();
    return num;
}

int evaluate(const std::string& operand, int num1, int num2)
{
    if (operand == "+") return num1 + num2;
    else if (operand == "-") return num1 - num2;
    else if (operand == "*") return num1 * num2;
    else if (operand == "/") return num1 / num2;
    else throw std::runtime_error("");
}

int calculatePolishNot(const std::vector<std::string>& polishNot)
{
    std::stack<int> numbers{};
    
    for (const auto& str : polishNot)
    {
        try
        {
            numbers.push(std::stoi(str));
        }
        catch (const std::invalid_argument&)
        {
            numbers.push(evaluate(str, pop(numbers), pop(numbers)));
        }
    }
    
    return pop(numbers);
}

int main()
{
    // std::vector<std::string> polishNot = {"2", "1", "+", "3", "*"};
    std::vector<std::string> polishNot = {"4", "13", "5", "/", "+"};
    std::cout << calculatePolishNot(polishNot);
}
