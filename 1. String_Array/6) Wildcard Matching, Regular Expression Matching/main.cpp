#include <iostream>
#include <string>

bool isMatchedWidecard(const std::string&, const std::string&, unsigned, unsigned);

bool isMatchedAfterStar(const std::string& str, const std::string& wildcard, unsigned strIdx, unsigned wildcardIdx)
{
    for (unsigned i = strIdx; i < str.size(); i++)
    {
        if (isMatchedWidecard(str, wildcard, i, wildcardIdx)) return true;
    }
    return false;
}

bool isMatchedIgnoringSrtars(const std::string& str, const std::string& wildcard, unsigned strIdx, unsigned wildcardIdx)
{
    for (unsigned i = wildcardIdx; i < wildcard.size(); i++)
    {
        if (wildcard[i] != '*')
        {
            return isMatchedAfterStar(str, wildcard, strIdx, i);
        }
    }
    return true;
}

bool isMatchedWidecard(const std::string& str, const std::string& wildcard, unsigned strIdx = 0, unsigned wildcardIdx = 0)
{
    for (unsigned i = wildcardIdx; i < wildcard.size(); i++)
    {
        if (wildcard[i] == '*') return isMatchedIgnoringSrtars(str, wildcard, strIdx, i);
        
        if (strIdx > str.size()) return false;
        
        if (wildcard[i] == '?' or str[strIdx] == wildcard[i]) strIdx++;
        else return false;
    }
    return true;
}

int main()
{
    std::string str = "abcdef";
    std::string wildcard = "**c?ef*";
    
    std::cout << "is wildcard matched: " << std::boolalpha << isMatchedWidecard(str, wildcard) << std::endl;
    return 0;
}
