#include <iostream>
#include <list>
#include <cmath>
#include <algorithm>

void print(const std::list<unsigned>& list)
{
    for (unsigned el : list) std::cout << el << " ";
    std::cout << std::endl;
}

std::list<unsigned> intersectionOfTwoLists(const std::list<unsigned>& list1,
                                           const std::list<unsigned>& list2)
{
    auto it1 = list1.cbegin();
    auto it2 = list2.cbegin();

    unsigned diff = std::abs(int(list1.size() - list2.size()));
    for (unsigned i = 0; i < diff; i++)
         if (list1.size() > list2.size()) it1++;
         else it2++;
    
    bool isIntersection = false;
    auto itIntersectionStart = it1;
    while (it1 != list1.cend() and it2 != list2.cend())
    {
        if (*it1 == *it2)
        {
            if (not isIntersection) itIntersectionStart = it1;
            isIntersection = true;
        }
        else
            isIntersection == false;
        
        it1++;
        it2++;
    }

    std::list<unsigned> result{};
    if (isIntersection)
        std::copy(itIntersectionStart, list1.cend(), std::back_inserter(result));
    return result;
}

int main()
{
    std::list<unsigned> list1 = {   1, 2, 7, 5, 3};
    std::list<unsigned> list2 = {3, 2, 5, 7, 5, 3};
    std::list<unsigned> intersection = intersectionOfTwoLists(list1, list2);

    std::cout << "list 1:" << std::endl;
    print(list1);
    std::cout << "list 2:" << std::endl;
    print(list2);
    std::cout << "intersection:" << std::endl;
    print(intersection);

    return 0;
}
