#include <iostream>
#include <vector>

using ListOfIterators = std::vector<unsigned>;

bool hasCycle(const ListOfIterators& list)
{
    unsigned slow = list[0];
    unsigned fast = list[0];
    
    while (list[fast] != list.size() and list[list[fast]] != list.size())
    {
        slow = list[slow];
        fast = list[list[fast]];
        
        if (slow == fast) return true;
    }
    
    return false;
}

int main()
{
    ListOfIterators list = {1, 2, 3, 4, 5};
    std::cout << "has cycle: " << std::boolalpha << hasCycle(list) << std::endl;
    list.back() = 2;
    std::cout << "has cycle: " << std::boolalpha << hasCycle(list) << std::endl;

    return 0;
}
