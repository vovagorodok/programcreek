#include <iostream>
#include <list>

void print(const std::list<unsigned>& list)
{
    for (unsigned el : list) std::cout << el << " ";
    std::cout << std::endl;
}

std::list<unsigned> numbersSum(const std::list<unsigned>& number1,
                               const std::list<unsigned>& number2)
{
    std::list<unsigned> result{};
    unsigned carry = 0;
    unsigned sum;
    
    auto it1 = number1.cbegin();
    auto it2 = number2.cbegin();
    
    while (it1 != number1.cend() or it2 != number2.cend())
    {
        sum = carry;
        
        if (it1 != number1.cend())
        {
            sum += *it1;
            it1++;
        }
        if (it2 != number2.cend())
        {
            sum += *it2;
            it2++;
        }

        carry = sum / 10;
        result.push_back(sum % 10);

    }
    
    if (carry) result.push_back(carry);
    
    return result;
}

int main()
{
    std::list<unsigned> number1 = {2, 4, 3};
    std::list<unsigned> number2 = {5, 6, 4};
    std::list<unsigned> sum = numbersSum(number1, number2);

    std::cout << "number 1:" << std::endl;
    print(number1);
    std::cout << "number 2:" << std::endl;
    print(number2);
    std::cout << "sum:" << std::endl;
    print(sum);

    return 0;
}
