#include <iostream>
#include <list>
#include <unordered_map>

template <typename T>
class LRUCache
{
    using Queue = std::list<T>;
    Queue queue;
    std::unordered_map<T, typename Queue::iterator> map;
    unsigned maxSize;
    
public:
    LRUCache(unsigned n) : maxSize(n) {}

    void refer(T x)
    {
        if (map.find(x) == map.end())
        {
            if (queue.size() == maxSize)
            { 
                T last = queue.back();
                queue.pop_back();
                map.erase(last);
            }
        }
        else
            queue.erase(map[x]);

        queue.push_front(x);
        map[x] = queue.begin();
    } 
      
    void display() const
    {
        for (auto el : queue)
            std::cout << el << " ";
        std::cout << std::endl;
    }
};

int main()
{
    LRUCache<int> cache(4);
  
    cache.refer(1);
    cache.refer(2);
    cache.refer(3);
    cache.refer(1);
    cache.refer(4);
    cache.refer(5);
    cache.display();

    return 0;
}
